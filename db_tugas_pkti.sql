-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 11 Jan 2020 pada 14.52
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_tugas_pkti`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `akg`
--

CREATE TABLE `akg` (
  `No` varchar(10) NOT NULL,
  `GolUsia` varchar(20) DEFAULT NULL,
  `BB_kg` int(11) DEFAULT NULL,
  `TB_cm` int(11) DEFAULT NULL,
  `Energi_kkal` int(11) DEFAULT NULL,
  `Protein_g` int(11) DEFAULT NULL,
  `Lemak_g` int(11) DEFAULT NULL,
  `Karbo_g` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `categories`
--

CREATE TABLE `categories` (
  `id_cat` int(11) NOT NULL,
  `cat_slug` varchar(100) NOT NULL,
  `cat_name` varchar(50) NOT NULL,
  `cat_desc` text NOT NULL,
  `cat_parent` int(11) NOT NULL,
  `cat_type` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `categories`
--

INSERT INTO `categories` (`id_cat`, `cat_slug`, `cat_name`, `cat_desc`, `cat_parent`, `cat_type`) VALUES
(7, 'makanan', 'Makanan', 'Kuliner berupa makanan', 0, 'kuliner'),
(8, 'minuman', 'Minuman', 'Kuliner berupa minuman', 0, 'kuliner'),
(9, 'halal', 'Halal', 'Kuliner berupa makanan halal', 0, 'kuliner'),
(10, 'non-halal', 'Non-Halal', 'Kuliner berupa makanan non-halal', 0, 'kuliner'),
(11, 'vegetarian', 'Vegetarian', 'Kuliner berupa makanan vegetarian ( tanpa daging)', 0, 'kuliner'),
(12, 'homepage-slider', 'Homepage Slider', 'ini adalah homapage slider', 0, 'slider'),
(15, 'info-makanan', 'Info Makanan', 'category tentang info makanan', 0, 'category');

-- --------------------------------------------------------

--
-- Struktur dari tabel `faq`
--

CREATE TABLE `faq` (
  `id_faq` int(11) NOT NULL,
  `faq_title` varchar(100) NOT NULL,
  `faq_ts` varchar(100) NOT NULL,
  `faq_date` date NOT NULL,
  `faq_age` varchar(100) NOT NULL,
  `faq_content` text NOT NULL,
  `faq_answer` text NOT NULL DEFAULT '',
  `faq_status` varchar(20) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `faq`
--

INSERT INTO `faq` (`id_faq`, `faq_title`, `faq_ts`, `faq_date`, `faq_age`, `faq_content`, `faq_answer`, `faq_status`) VALUES
(1, 'Pola Hidup Sehat Ibu Hamil ', 'Rose', '2020-01-11', '29', 'Dok, Apa iya rutinitas jalan kaki saat hamil juga perlu disamping makan makanan bergizi bagi bayi ? Berapa lama durasi jalan kaki yang dianjurkan per hari? ', '#', 'pending');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kuliner`
--

CREATE TABLE `kuliner` (
  `ID_Kul` varchar(10) NOT NULL,
  `Nama` varchar(100) NOT NULL,
  `UrlGambar` varchar(200) NOT NULL,
  `JlhView` int(11) DEFAULT NULL,
  `FaktaUnik` varchar(1000) DEFAULT NULL,
  `Bumil` varchar(1) DEFAULT NULL,
  `Diabetes` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kuliner`
--

INSERT INTO `kuliner` (`ID_Kul`, `Nama`, `UrlGambar`, `JlhView`, `FaktaUnik`, `Bumil`, `Diabetes`) VALUES
('F001', 'Nasi Goreng', 'https://blue.kumparan.com/image/upload/fl_progressive,fl_lossy,c_fill,q_auto:best,w_640/v1515557737/asxtrr2ga1os4abfmuoe.jpg', 0, 'Berkalori Tinggi, Berbahaya jika dimakan bersama Mentimun', 'Y', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengguna`
--

CREATE TABLE `pengguna` (
  `id_user` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email_user` varchar(100) NOT NULL,
  `password_user` varchar(32) DEFAULT NULL,
  `fullname_user` varchar(100) NOT NULL,
  `id_role` int(11) NOT NULL,
  `pengguna_status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pengguna`
--

INSERT INTO `pengguna` (`id_user`, `username`, `email_user`, `password_user`, `fullname_user`, `id_role`, `pengguna_status`) VALUES
(1, 'superuser', 'superuser@gmail.com', '0baea2f0ae20150db78f58cddac442a9', 'Super User', 1, 1),
(4, 'yienjesslyn', 'yienjesslyn@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', 'Jesslyn Yien', 0, 0),
(5, 'yienjesslyn', 'yienjesslyn@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '', 2, 1),
(6, 'rose', 'rose@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '', 2, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjual`
--

CREATE TABLE `penjual` (
  `ID_Seller` varchar(10) NOT NULL,
  `Nama` varchar(100) NOT NULL,
  `Alamat` varchar(100) NOT NULL,
  `LinkPeta` varchar(500) DEFAULT NULL,
  `LinkGbr` varchar(500) DEFAULT NULL,
  `NoTelp` varchar(20) DEFAULT NULL,
  `Waktu` varchar(100) DEFAULT NULL,
  `Rating` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjual_kuliner`
--

CREATE TABLE `penjual_kuliner` (
  `ID_Kul` varchar(10) DEFAULT NULL,
  `ID_Seller` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `post`
--

CREATE TABLE `post` (
  `id_post` int(11) NOT NULL,
  `post_title` varchar(200) DEFAULT NULL,
  `post_slug` varchar(200) DEFAULT NULL,
  `post_content` text DEFAULT NULL,
  `post_date` date DEFAULT NULL,
  `post_author` varchar(100) DEFAULT NULL,
  `post_type` varchar(100) DEFAULT NULL,
  `post_thumbnail` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `post`
--

INSERT INTO `post` (`id_post`, `post_title`, `post_slug`, `post_content`, `post_date`, `post_author`, `post_type`, `post_thumbnail`) VALUES
(18, 'Tentang Kami', 'tentang-kami', '<p>Kami ada tim 7 era modern.</p>', '2020-01-07', '1', 'page', NULL),
(23, 'Sate Padang', 'sate-padang', '', '2020-01-09', '1', 'kuliner', 'sate_padang.jpg'),
(25, 'Rutin Jalan Kaki Saat Hamil, Bunda Akan Merasakan 5 Manfaat ini', 'rutin-jalan-kaki-saat-hamil-bunda-akan-merasakan-5-manfaat-ini', '<p><strong><em>Dok, Apa iya rutinitas jalan kaki saat hamil juga perlu disamping makan makanan bergizi bagi bayi ? Berapa lama durasi jalan kaki yang dianjurkan per hari?</em></strong></p>\r\n<p><strong><em>Rose, 29 tahun</em></strong></p>\r\n<p>Jalan kaki saat hamil mungkin tak terpikirkan untuk dilakukan oleh Bunda karena sudah merasa kelelahan membawa bayi dalam kandungan. Padahal, jalan kaki amat direkomendasikan untuk dilakukan selama kehamilan dan baik bagi janin dalam kandungan</p>\r\n<p>Tak perlu lama-lama, cukup lakukan 30 menit setiap hari. Anda bisa berjalan kaki di taman dekat rumah atau mengitari komplek perumahan. Jangan lupa ajak pak suami untuk olahraga bersama, ya, Bun!</p>\r\n<p><span style=\"font-size: 14pt;\"><strong>Manfaat jalan kaki saat hamil #1: Mengurangi risiko diabetes</strong></span></p>\r\n<p><span style=\"font-size: 12pt;\">Kadar gula darah umumnya akan meningkat selama kehamilan, apalagi untuk Bunda yang ngidam makanan manis selama hamil. Jalan kaki saat hamil bisa mengurangi risiko penyakit ini.</span></p>\r\n<p><span style=\"font-size: 12pt;\">Tak hanya risiko diabetes, berjalan kaki ketika hamil akan membantu menjaga berat badan Bunda dan janin dalam kandungan lahir dengan berat badan normal.</span></p>\r\n<p><span style=\"font-size: 14pt;\"><strong>Manfaat jalan kaki saat hamil #2: Meminimalkan risiko preeklamsia</strong></span></p>\r\n<p><span style=\"font-size: 12pt;\">Preeklamsia biasanya ditandai tekanan darah yang tinggi dan adanya protein dalam urin, dan dengan jalan kaki risiko ini bisa dikurangi risikonya.</span></p>\r\n<p><span style=\"font-size: 12pt;\">kaki saat hamil akan membantu menjaga berat badan bunda tetap ideal, mengurangi kadar kolesterol sehingga kesehatan kehamilan tetap terjaga</span></p>\r\n<p><span style=\"font-size: 14pt;\"><strong>Manfaat jalan kaki saat hamil #3: Memudahkan proses persalinan</strong></span></p>\r\n<p><span style=\"font-size: 12pt;\">alan kaki saat hamil juga akan memudahkan Bunda melalui proses persalinan, apalagi jika Anda ingin lahiran normal (vaginal birth). Dengan berjalan kaki, otot akan semakin kuat setiap waktu, sehingga persalinan akan terasa cepat dan mudah</span></p>\r\n<p><span style=\"font-size: 14pt;\"><strong>Manfaat jalan kaki saat hamil #4: Mengurangi stres</strong></span></p>\r\n<p><span style=\"font-size: 12pt;\">Saat hamil merupakan hal yang wajar terjadi akibat perubahan hormon yang memengaruhi suasana hati. Mulai dari bahagia hingga tiba-tiba berubah menjadi gelisah bahkan depresi.</span></p>\r\n<p><span style=\"font-size: 12pt;\">Jalan kaki saat hamil akan memproduksi hormon endorfin, hormon yang akan mengubah mood Bunda menjadi lebih baik sepanjang kehamilan.</span></p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"font-size: 12pt;\">Menurut the Physical Activity Guidelines for Americans dan NHS, durasi jalan kaki saat hamil yang dianjurkan yaitu 150 menit dalam seminggu. Bunda bisa berjalan kaki 45-60 menit pada dua trimester pertama lalu 30 menit saat kehamilan sudah memasuki trimester ketiga</span></p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"font-size: 12pt;\">lakukan sesuai kemampuan Anda ya, Bun. Jangan langsung paksakan untuk melakukannya setiap hari, tetapi coba bertahap saja. Kenakan alas kaki yang nyaman dan jangan lupa selalu membawa air putih agar tubuh Anda selalu terhidrasi.</span></p>\r\n<p><strong>oleh : dr.Giasinta</strong></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', '2020-01-11', '1', 'post', 'bumil.jpg'),
(26, 'Yang Penting Gizi Seimbang, Bukan \"4 Sehat 5 Sempurna\"', 'yang-penting-gizi-seimbang-bukan-4-sehat-5-sempurna', '<p><span style=\"color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\">Kita telah mengenal jargon 4 sehat 5 sempurna jauh sebelum kita benar-benar belajar formal tentang makanan sehat di bangku sekolah. Slogan 4 sehat 5 sempurna ini dipopulerkan oleh guru besar ilmu gizi pertama di Indonesia, Prof. Poerwo Soedarmo pada tahun 1950-an.</span><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><span style=\"color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\">Dalam konsep 4 sehat 5 sempurna, makanan sehat adalah makanan yang mengandung 4 sumber nutrisi yaitu makanan pokok, lauk pauk, sayur-sayuran, buah-buahan, dan disempurnakan dengan susu.</span><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><span style=\"color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\">Sejak tahun 1990-an pedoman 4 sehat 5 sempurna ini dianggap tak lagi sesuai dengan perkembangan ilmu pengetahuan dan teknologi gizi. Hingga kemudian, pemerintah melalui Kementerian Kesehatan akhir bulan Oktober lalu mengkampanyekan slogan \"Isi Piringku\" sebagai pengganti slogan \"4 Sehat 5 Sempurna\" untuk pedoman konsumsi sehari-hari dalam memenuhi gizi seimbang.</span><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><span style=\"color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\">\"Dulu kita punya slogan 4 Sehat 5 Sempurna, namun dalam perkembangan ilmu gizi tidak cukup tepat untuk mengakomodir perkembangan ilmu yang baru. Kalau hanya bicara 4 Sehat 5 Sempurna tanpa keseimbangan itu tidak cukup,\" kata Direktur Jenderal Kesehatan Masyarakat Kementerian Kesehatan</span><a style=\"box-sizing: border-box; color: #0098da; text-decoration-line: none; background-color: #ffffff; touch-action: manipulation; font-family: \'PT Sans\', sans-serif; font-size: 16px;\" href=\"http://www.antaranews.com/berita/661996/isi-piringku-gantikan-4-sehat-5-sempurna\" target=\"_blank\" rel=\"noopener\">&nbsp;Anung Sugihantono&nbsp;</a><span style=\"color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\">dalam konferensi pers acara Forum Pangan Asia Pasifik.</span><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><span style=\"color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\">Secara umum, \"Isi Piringku\" menggambarkan porsi makan yang dikonsumsi dalam satu piring yang terdiri dari 50 persen buah dan sayur, dan 50 persen sisanya terdiri dari karbohidrat dan protein.</span><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><span style=\"color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\">Kampanye \"Isi Piringku\" juga menekankan untuk membatasi gula, garam, dan lemak dalam konsumsi sehari-hari. Dalam perkembangan ilmu gizi yang baru, pedoman \"4 Sehat 5 Sempurna\" berubah menjadi pedoman gizi seimbang yang terdiri dari 10 pesan tentang menjaga gizi.</span><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><span style=\"color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\">Dari 10 pesan tersebut, Anung mengelompokkan lagi menjadi empat pesan pokok yakni pola makan gizi seimbang, minum air putih yang cukup, aktivitas fisik minimal 30 menit per hari, dan mengukur tinggi dan berat badan yang sesuai untuk mengetahui kondisi tubuh.</span></p>\r\n<p><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><span style=\"color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\">Selain diagram \"Isi Piringku\" yang telah disebutkan, kampanye tersebut juga menekankan empat hal penting lainnya yaitu cuci tangan sebelum makan, aktivitas fisik yang cukup, minum air putih cukup, dan memantau tinggi badan dan berat badan.</span><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><span style=\"box-sizing: border-box; font-weight: bolder; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\"><br style=\"box-sizing: border-box;\" /></span><span style=\"box-sizing: border-box; font-weight: bolder; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\">Baca juga:</span><span style=\"color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\">&nbsp;</span><a style=\"box-sizing: border-box; color: #0098da; text-decoration-line: none; background-color: #ffffff; touch-action: manipulation; font-family: \'PT Sans\', sans-serif; font-size: 16px;\" href=\"https://tirto.id/berapa-banyak-gula-dalam-susu-kental-manis-cw6V\" target=\"_blank\" rel=\"noopener\">Berapa Banyak Gula dalam Susu Kental Manis?</a><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><span style=\"color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\">Rio Jati Kusuma, dosen departemen gizi dan kesehatan Fakultas Kedokteran Universitas Gadjah Mada, kepada&nbsp;</span><em style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\">Tirto&nbsp;</em><span style=\"color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\">menyatakan bahwa konsep 4 sehat 5 sempurna dulu dicanangkan dalam rangka membuat rekomendasi diet sehat untuk masyarakat Indonesia.</span><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><span style=\"color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\">Karena saat itu standar menu sehat masih belum ada acuannya, dibuatlah konsep 4 sehat 5 sempurna, yang bertujuan mengenalkan bahwa di dalam piring minimal harus terdapat sumber karbohidrat, protein, dan lemak dari makanan pokok, sayur, lauk hewani, lauk nabati, dan susu sebagai pelengkapnya.</span><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><span style=\"color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\">Rio Jati merasa konsep ini kurang pas bila diterapkan saat ini karena ketiadaan standar dalam penentuan jumlah setiap bahan makanan pada konsep 4 sehat 5 sempurna. Akibatnya, setiap orang bisa saja menafsirkan menu yang mereka makan dalam porsi dan kadar yang beraneka macam, asal telah memenuhi syarat 4 sehat lima sempurna: makanan pokok, lauk pauk, sayur-sayuran, buah-buahan, dan disempurnakan dengan susu.</span></p>\r\n<p><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><br style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\" /><span style=\"color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\">&ldquo;Dampaknya adalah peningkatan jumlah konsumsi lemak dan energi yang berpotensi menyebabkan obesitas dan kegemukan. Karena jumlahnya yang tidak standard, karena masing-masing individu bisa menafsirkan secara beragam,&rdquo; jelasnya kepada&nbsp;</span><em style=\"box-sizing: border-box; color: #212529; font-family: \'PT Sans\', sans-serif; font-size: 16px; background-color: #ffffff;\">Tirto.</em></p>\r\n<p>&nbsp;</p>', '2020-01-11', '1', 'post', 'bg7.jpg'),
(29, 'Sate Padang', 'sate-padang', '<p>&nbsp;</p>\r\n<p>Sate Padang adalah sebutan untuk tiga jenis varian sate di Sumatra Barat, yaitu Sate Padang, Sate Padang Panjang dan Sate Pariaman.</p>\r\n<p>Sate Padang memakai bahan daging sapi, lidah, atau jerohan (jantung, usus, dan tetelan) dengan bumbu kuah kacang kental (mirip bubur) ditambah cabai yang banyak sehingga rasanya pedas.</p>', '2020-01-11', '1', 'slider', 'sate_padang3.jpg'),
(31, 'Bihun Bebek', 'bihun-bebek', '<p>Bihun Bebek ini adalah kuliner dari suku Tionghua di Medan, dimana bihun dimasak dengan ramuan obat dan daging bebek yang membuatnya lezat sekaligus sangat bergizi.&nbsp;</p>', '2020-01-11', '1', 'slider', 'bihun_bebek1.jpg'),
(33, 'Pancake Durian', 'pancake-durian', '<p>Kuliner ini adalah oleh oleh khas Medan yang terbuat dari buah Durian, dinikmati dalam keadaan dingin. Terasa lembut dan sangat lezat. Pasti bikin nagih .&nbsp;</p>', '2020-01-11', '1', 'slider', 'Pancake-Durian.png'),
(34, 'Otak Otak', 'otak-otak', '', '2020-01-11', '1', 'kuliner', 'otakotak.jpg'),
(35, 'Es Teler', 'es-teler', '', '2020-01-11', '1', 'kuliner', 'escampur.jpg'),
(36, 'Soto Ayam ', 'soto-ayam', '', '2020-01-11', '1', 'kuliner', 'sotoay.jpg'),
(37, 'Mie Pangsit ', 'mie-pangsit', '', '2020-01-11', '1', 'kuliner', 'pangsit.png'),
(40, 'Mie Bakso', 'mie-bakso', '', '2020-01-11', '1', 'kuliner', 'miebakso.jpg'),
(41, 'Nasi Goreng', 'nasi-goreng', '', '2020-01-11', '1', 'kuliner', 'nasgor.jpg'),
(42, 'Bihun Bebek', 'bihun-bebek', '', '2020-01-11', '1', 'kuliner', 'bihun_bebek2.jpg'),
(43, 'Sup Krim Jagung', 'sup-krim-jagung', '', '2020-01-11', '1', 'kuliner', 'supkrim.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `post_meta`
--

CREATE TABLE `post_meta` (
  `id_post_meta` int(11) NOT NULL,
  `id_post` int(11) NOT NULL,
  `post_meta_name` text NOT NULL,
  `post_meta_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `post_meta`
--

INSERT INTO `post_meta` (`id_post_meta`, `id_post`, `post_meta_name`, `post_meta_value`) VALUES
(1, 21, 'kalori', '100'),
(2, 21, 'kalori', '100'),
(3, 21, 'lemak', '3'),
(4, 21, 'karbohidrat', '3'),
(5, 21, 'protein', '3'),
(6, 2, 'kalori', '10'),
(7, 2, 'lemak', '20'),
(8, 2, 'karbohidrat', '30'),
(9, 2, 'protein', '40'),
(10, 2, 'rating', '5'),
(11, 5, 'kalori', ''),
(12, 5, 'lemak', ''),
(13, 5, 'karbohidrat', ''),
(14, 5, 'protein', ''),
(15, 5, 'rating', ''),
(16, 6, 'kalori', ''),
(17, 6, 'lemak', ''),
(18, 6, 'karbohidrat', ''),
(19, 6, 'protein', ''),
(20, 6, 'rating', ''),
(21, 19, 'kalori', ''),
(22, 19, 'lemak', ''),
(23, 19, 'karbohidrat', ''),
(24, 19, 'protein', ''),
(25, 19, 'rating', ''),
(26, 14, 'btn_cta_name', 'SEE RECEIPT'),
(27, 14, 'btn_cta_url', '#'),
(28, 14, 'btn_cta_target', '_self'),
(29, 14, 'btn_cta_name', 'SEE RECEIPT'),
(30, 14, 'btn_cta_url', '#'),
(31, 14, 'btn_cta_target', '_self'),
(32, 15, 'btn_cta_name', 'LIHAT'),
(33, 15, 'btn_cta_url', '#'),
(34, 15, 'btn_cta_target', '_self'),
(35, 15, 'btn_cta_name', 'LIHAT'),
(36, 15, 'btn_cta_url', '#'),
(37, 15, 'btn_cta_target', '_self'),
(38, 16, 'btn_cta_name', 'See Receipe'),
(39, 16, 'btn_cta_url', '#'),
(40, 16, 'btn_cta_target', '_self'),
(41, 16, 'btn_cta_name', 'See Receipe'),
(42, 16, 'btn_cta_url', '#'),
(43, 16, 'btn_cta_target', '_self'),
(44, 15, 'btn_cta_name', 'LIHAT'),
(45, 15, 'btn_cta_url', '#'),
(46, 15, 'btn_cta_target', '_self'),
(47, 23, 'kalori', '161'),
(48, 23, 'lemak', '6.63'),
(49, 23, 'karbohidrat', '6.79'),
(50, 23, 'protein', '18.01'),
(51, 23, 'rating', '4'),
(52, 14, 'btn_cta_name', 'Lihat '),
(53, 14, 'btn_cta_url', '#'),
(54, 14, 'btn_cta_target', '_self'),
(55, 16, 'btn_cta_name', 'Lihat '),
(56, 16, 'btn_cta_url', '#'),
(57, 16, 'btn_cta_target', '_self'),
(58, 16, 'btn_cta_name', 'Lihat '),
(59, 16, 'btn_cta_url', '#'),
(60, 16, 'btn_cta_target', '_self'),
(61, 23, 'kalori', '161'),
(62, 23, 'lemak', '6.63'),
(63, 23, 'karbohidrat', '6.79'),
(64, 23, 'protein', '18.01'),
(65, 23, 'rating', '0'),
(66, 27, 'kalori', '169'),
(67, 27, 'lemak', '2.63'),
(68, 27, 'karbohidrat', '3.79'),
(69, 27, 'protein', '18.01'),
(70, 27, 'rating', '4'),
(71, 28, 'btn_cta_name', 'Lihat '),
(72, 28, 'btn_cta_url', ''),
(73, 28, 'btn_cta_target', '_self'),
(74, 29, 'btn_cta_name', 'Lihat '),
(75, 29, 'btn_cta_url', ''),
(76, 29, 'btn_cta_target', '_self'),
(77, 29, 'btn_cta_name', 'Lihat '),
(78, 29, 'btn_cta_url', ''),
(79, 29, 'btn_cta_target', '_self'),
(80, 29, 'btn_cta_name', 'Lihat '),
(81, 29, 'btn_cta_url', ''),
(82, 29, 'btn_cta_target', '_self'),
(83, 29, 'btn_cta_name', 'Lihat '),
(84, 29, 'btn_cta_url', ''),
(85, 29, 'btn_cta_target', '_self'),
(86, 30, 'btn_cta_name', 'Lihat '),
(87, 30, 'btn_cta_url', ''),
(88, 30, 'btn_cta_target', '_self'),
(89, 31, 'btn_cta_name', 'Lihat '),
(90, 31, 'btn_cta_url', ''),
(91, 31, 'btn_cta_target', '_self'),
(92, 30, 'btn_cta_name', 'Lihat '),
(93, 30, 'btn_cta_url', ''),
(94, 30, 'btn_cta_target', '_self'),
(95, 30, 'btn_cta_name', 'Lihat '),
(96, 30, 'btn_cta_url', ''),
(97, 30, 'btn_cta_target', '_self'),
(98, 29, 'btn_cta_name', 'Lihat '),
(99, 29, 'btn_cta_url', ''),
(100, 29, 'btn_cta_target', '_self'),
(101, 32, 'btn_cta_name', 'Lihat '),
(102, 32, 'btn_cta_url', ''),
(103, 32, 'btn_cta_target', '_self'),
(104, 33, 'btn_cta_name', 'Lihat '),
(105, 33, 'btn_cta_url', ''),
(106, 33, 'btn_cta_target', '_self'),
(107, 34, 'kalori', '161'),
(108, 34, 'lemak', '2.63'),
(109, 34, 'karbohidrat', '3.79'),
(110, 34, 'protein', '18.01'),
(111, 34, 'rating', '4'),
(112, 34, 'kalori', '161'),
(113, 34, 'lemak', '2.63'),
(114, 34, 'karbohidrat', '3.79'),
(115, 34, 'protein', '18.01'),
(116, 34, 'rating', '4'),
(117, 35, 'kalori', '169'),
(118, 35, 'lemak', '6.45'),
(119, 35, 'karbohidrat', '3.79'),
(120, 35, 'protein', '18.01'),
(121, 35, 'rating', '5'),
(122, 36, 'kalori', '161'),
(123, 36, 'lemak', '6.63'),
(124, 36, 'karbohidrat', '3.79'),
(125, 36, 'protein', '18.01'),
(126, 36, 'rating', '5'),
(127, 37, 'kalori', '168'),
(128, 37, 'lemak', '2.63'),
(129, 37, 'karbohidrat', '29,7'),
(130, 37, 'protein', '18.01'),
(131, 37, 'rating', '5'),
(132, 38, 'kalori', '161'),
(133, 38, 'lemak', '2.63'),
(134, 38, 'karbohidrat', '6.79'),
(135, 38, 'protein', '18.01'),
(136, 38, 'rating', '4'),
(137, 38, 'kalori', '161'),
(138, 38, 'lemak', '2.63'),
(139, 38, 'karbohidrat', '6.79'),
(140, 38, 'protein', '18.01'),
(141, 38, 'rating', '4'),
(142, 39, 'kalori', '161'),
(143, 39, 'lemak', '2.63'),
(144, 39, 'karbohidrat', '3.79'),
(145, 39, 'protein', '18.01'),
(146, 39, 'rating', '4'),
(147, 40, 'kalori', '161'),
(148, 40, 'lemak', '6.63'),
(149, 40, 'karbohidrat', '3.79'),
(150, 40, 'protein', '18.01'),
(151, 40, 'rating', '4'),
(152, 41, 'kalori', '161'),
(153, 41, 'lemak', '2.63'),
(154, 41, 'karbohidrat', '3.79'),
(155, 41, 'protein', '18.01'),
(156, 41, 'rating', '4'),
(157, 42, 'kalori', '169'),
(158, 42, 'lemak', '2.63'),
(159, 42, 'karbohidrat', '3.79'),
(160, 42, 'protein', '18.01'),
(161, 42, 'rating', '5'),
(162, 43, 'kalori', '161'),
(163, 43, 'lemak', '2.63'),
(164, 43, 'karbohidrat', '3.79'),
(165, 43, 'protein', '18.01'),
(166, 43, 'rating', '2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `post_taxonomy`
--

CREATE TABLE `post_taxonomy` (
  `id_post` int(11) DEFAULT NULL,
  `id_cat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `post_taxonomy`
--

INSERT INTO `post_taxonomy` (`id_post`, `id_cat`) VALUES
(23, 7),
(23, 9),
(28, 12),
(31, 12),
(30, 12),
(29, 12),
(33, 12),
(34, 7),
(34, 9),
(35, 8),
(35, 9),
(35, 11),
(36, 7),
(36, 9),
(37, 7),
(37, 10),
(40, 7),
(40, 9),
(41, 7),
(41, 9),
(41, 11),
(42, 7),
(42, 9),
(43, 7),
(43, 9),
(43, 11);

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id_role` int(11) NOT NULL,
  `role_name` varchar(30) NOT NULL,
  `role_desc` text NOT NULL,
  `role_cap` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id_role`, `role_name`, `role_desc`, `role_cap`) VALUES
(1, 'Administrator', '', 99),
(2, 'Member', ' ', 30);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `akg`
--
ALTER TABLE `akg`
  ADD PRIMARY KEY (`No`);

--
-- Indeks untuk tabel `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id_cat`);

--
-- Indeks untuk tabel `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id_faq`);

--
-- Indeks untuk tabel `kuliner`
--
ALTER TABLE `kuliner`
  ADD PRIMARY KEY (`ID_Kul`);

--
-- Indeks untuk tabel `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id_user`);

--
-- Indeks untuk tabel `penjual`
--
ALTER TABLE `penjual`
  ADD PRIMARY KEY (`ID_Seller`);

--
-- Indeks untuk tabel `penjual_kuliner`
--
ALTER TABLE `penjual_kuliner`
  ADD KEY `ID_Kul` (`ID_Kul`),
  ADD KEY `ID_Seller` (`ID_Seller`);

--
-- Indeks untuk tabel `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id_post`);

--
-- Indeks untuk tabel `post_meta`
--
ALTER TABLE `post_meta`
  ADD PRIMARY KEY (`id_post_meta`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_role`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `categories`
--
ALTER TABLE `categories`
  MODIFY `id_cat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `faq`
--
ALTER TABLE `faq`
  MODIFY `id_faq` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `post`
--
ALTER TABLE `post`
  MODIFY `id_post` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT untuk tabel `post_meta`
--
ALTER TABLE `post_meta`
  MODIFY `id_post_meta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `penjual_kuliner`
--
ALTER TABLE `penjual_kuliner`
  ADD CONSTRAINT `penjual_kuliner_ibfk_1` FOREIGN KEY (`ID_Kul`) REFERENCES `kuliner` (`ID_Kul`) ON DELETE CASCADE,
  ADD CONSTRAINT `penjual_kuliner_ibfk_2` FOREIGN KEY (`ID_Seller`) REFERENCES `penjual` (`ID_Seller`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
