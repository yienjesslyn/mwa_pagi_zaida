<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {


	public function index()
	{
		$data['_pageTitle'] = "Blog";
		$this->load->view('blog/DaftarBlog',$data);
    }
    
	public function artikel()
	{
		$data['_pageTitle'] = "Artikel";
		$this->load->view('blog/Artikel',$data);
	}
	
	public function artikel2()
	{
		$data['_pageTitle'] = "Artikel2";
		$this->load->view('blog/Artikel2',$data);
	}
}
