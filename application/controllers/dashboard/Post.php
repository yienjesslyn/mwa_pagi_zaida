<?php 



defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends CI_Controller {

    private $data = [];

    // MASUKIN NAMA TABLE YANG DI DATABASE
    private $table = 'post';

    // MASUKIN NAMA PRIMARY KEY YANG DARI DATABASE
    private $pk = 'id_post';

    private $post_type = [
        'page' => [
            'title' => 'Page',
            'taxonomy' => false,
        ],
        'post' => [
            'title' => 'Post',
            'taxonomy' => [
                'ID' => 'category',
                'name' => 'Category'
            ]
        ],

        'slider' => [
            'title' => 'Slider',
            'taxonomy' => [
                'ID' => 'slider',
                'name' => 'Slide'
            ]
        ],
        'kuliner' => [
            'title' => 'Kuliner',
            'taxonomy' => [
                'ID' => 'kuliner',
                'name' => 'Jenis Kuliner'
            ]
        ]

    ];

    public function __construct() {
        parent::__construct();

        $this->load->library('taxonomy');

        $this->data['register']['post_type'] = $this->post_type;
        
    }

    
    public function index()
    {
        $this->data['id'] = $this->input->get('id');
        $this->data['submit_url']  = 'dashboard/post/add';


        $this->db->select()->from($this->table);

        if($this->input->get('post_type')) $this->db->where('post_type', $this->input->get('post_type'));
        else $this->db->where('post_type', 'post');


        $post_types = $this->db->get();

        $this->data['data'] = $post_types;

        if($this->data['id']) 
        {

            $this->data['row'] = $this->find($this->data['id']);
            $this->data['submit_url'] = 'dashboard/post/update';
        }

        $this->load->view('admin/header');

        // HEADER SAMA FOOTER JANGAN DI GANTI 
        // YANG INI AJA, SESUAI KAN SAMA VIEW YANG DIBUAT DI FOLDER "APPLICAITON/VIEWS/ADMIN/{NAMA CIBTRIKKER}/NAMA FIILE"
        $this->load->view('admin/post/index', $this->data);
        $this->load->view('admin/footer');
    }

    public function find( $value ) {
        $query = $this->db->select()
            ->from($this->table)
            ->where($this->pk, $value)
            ->get();
        
        if($query->num_rows() > 0 ) {
            return $query->row();
        }

        return false;
    }

    public function delete()
    {

        $response = [];

        $id = $this->input->get('id');
        $find = $this->find($id);

    
        if($find) {
            
            // delete post
            $this->db->where($this->pk, $id);
            $this->db->delete($this->table);

            // delete taxonomy
            $this->db->where('id_post', $id);
            $this->db->delete('post_taxonomy');

            
            $response = [
                'code' => 200,
                'status' => 'success',
                'message' => 'Berhasil menghapus'
            ];

        }else {
            $response = [
                'code' => 400,
                'status' => 'warning',
                'message' => 'Tidak dapat menghapus'
            ];
        }

        $this->session->set_flashdata($response);
        
        
        //GANTI REDIRECT SESUAI CONTROLLERNYA
        redirect($this->agent->referrer());

    }

    public function add()
    {

        // GANTI INPUT SESUAI FORM 

        $title = $this->input->post('post_title');
        $slug = slugify($title);

        $thumbnail = $this->input->post('post_thumbnail');
        $do_upload = $this->form->upload('file_post_thumbnail');

        //var_dump($do_upload);

        if($do_upload) $thumbnail = $do_upload['file_name'];

        //die();

        $data = [
            
            'post_title'    => $title,            
            'post_slug'     => $slug,
            'post_content'  => $this->input->post('post_content'),
            'post_author'   => $this->input->post('post_author'),
            'post_type'     => $this->input->post('post_type'),  
            'post_date'     => date('Y-m-d'),
            'post_thumbnail' => $thumbnail
        ];

        $this->db->insert($this->table, $data);

        $id_post = $this->db->insert_id();
        

        $count_meta = count($this->input->post('meta'));
        if($count_meta > 0) {
            foreach($this->input->post('meta') as $key => $value) {
                $this->form->insert_meta($id_post, $key, $value);
            }
        }


        $id_cats = $this->input->post('id_cat');
        $this->taxonomy->save($id_post, $id_cats);

        /* $id_cats = $this->input->post('id_cat');
        if(count($id_cats) > 0) {
            for($i = 0; $i < count($id_cats); $i++)
            {
                $this->db->insert('post_taxonomy',  [
                    'id_post' => $id_post,
                    'id_cat' => $id_cats[$i]
                ]);
                
            }
        } */

        $response = [
            'code' => 200,
            'status' => 'success',
            'message' => 'Berhasil menambahkan'
        ];

        $this->session->set_flashdata( $response );
        
        
        //GANTI REDIRECT SESUAI CONTROLLERNYA
        redirect($this->agent->referrer());
    }

    public function update()
    {

        $id = $this->input->post('id');
        $find = $this->find($id);

        if($find) 
        {
            
            // GANTI INPUT SESUAI FORM
            $title = $this->input->post('post_title');
            $slug = slugify($title);

            
            $thumbnail = $this->input->post('post_thumbnail');
            $do_upload = $this->form->upload('file_post_thumbnail');

            if($do_upload) $thumbnail = $do_upload['file_name'];

            //die();

            $data = [
                
                'post_title'    => $title,            
                'post_slug'     => $slug,
                'post_content'  => $this->input->post('post_content'),
                'post_author'   => $this->input->post('post_author'),
                'post_type'     => $this->input->post('post_type'),  
                'post_thumbnail' => $thumbnail
            ];

            $this->db->where($this->pk, $id);
            $this->db->update($this->table, $data);

            // get all category
            $id_cats = $this->input->post('id_cat');

            $count_meta = count($this->input->post('meta'));
            if($count_meta > 0) {
                foreach($this->input->post('meta') as $key => $value) {
                    $this->form->insert_meta($id, $key, $value);
                }
            }

            $this->taxonomy->save($id, $id_cats);

            $response = [
                'code' => 200,
                'status' => 'success',
                'message' => 'Berhasil memperbaharui'
            ];

        }
        else {

            $response = [
                'code' => 400,
                'status' => 'warning',
                'message' => 'Data tidak dapat ditemukan'
            ];

        }

        //GANTI REDIRECT SESUAI CONTROLLERNYA
        redirect($this->agent->referrer());
    }

}

/* End of file Post.php */
