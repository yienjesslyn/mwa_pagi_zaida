<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {


    private $data = [];

    public function __construct() {
        parent::__construct();
    }

    public function index()
    {
     
        $this->load->view('admin/header');
        $this->load->view('admin/menu');
        $this->load->view('admin/footer');
        
    }

    public function add() {
        

        $data = $this->input->post();
        unset($data['add_menu']);

        $this->db->insert('menus', $data);

        redirect(base_url('dashboard/menu'));
    }

    public function delete() {

    }

    public function update() {

        $this->db->where('id_menu > ', 0);
        $this->db->delete('menus');

        $id_menu = $this->input->post('id_menu');

        //var_dump($id_menu);

        //die();

        if(count($id_menu) > 0) { 
            foreach($id_menu as $id) {
                $menu_name = $this->input->post('menu_name');
                $menu_url = $this->input->post('menu_url');
                $menu_parent = $this->input->post('menu_parent');
                $this->db->insert('menus', [
                    'menu_name' => $menu_name[$id],
                    'menu_url' => $menu_url[$id],
                    'menu_parent' => $menu_parent[$id],
                ]);

            }
        }


        
        redirect(base_url('dashboard/menu'));

    }

}

/* End of file Menu.php */
