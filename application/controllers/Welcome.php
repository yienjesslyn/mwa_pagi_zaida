<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	private $theme = 'default';
	private $data  = [];

	private $template = null;

	public function __construct() {
		parent::__construct();

	}

	public function get_template_directory_uri()
	{
		return 'themes/' . $this->theme;
	}

	public function get_header()
	{
		$this->load->view($this->get_template_directory_uri() . '/header', $this->data);
	}

	public function get_footer()
	{
		$this->load->view($this->get_template_directory_uri() . '/footer', $this->data);
	}

	public function index()
	{
		$this->data['_pageTitle'] = "Home";								
		$this->data['actve_menu'] = 'home';
		$this->get_header();
		$this->load->view($this->get_template_directory_uri() . '/index', $this->data);
		$this->get_footer();
	
	}

	public function login()
	{

	}

	public function register()
	{

		$rules = [
			[
				'label' => 'Username',
				'field' => 'username',
				'rules' => 'required'
			],
			[
				'label' => 'Password',
				'field' => 'password_user',
				'rules' => 'required'
			],
			[
				'label' => 'Re Type Password',
				'field' => 're_password_user',
				'rules' => 'required|matches[password]'
			],
			[
				'label' => 'Email',
				'field' => 'email',
				'rules' => 'required|valid_email'
			],
			[
				'label' => 'Agreement',
				'field' => 'agreement',
				'rules' => 'required'
			]
		];

		$run = $this->form_validation->set_rules($rules);

		if($run) {
			
			$data = [
				'username' => $this->input->post('username'),
				'email_user' => $this->input->post('email'),
				'password_user' => md5($this->input->post('password_user')),
				'id_role' => 2,
				'pengguna_status' => 1
			];

			$this->db->insert('pengguna', $data);

			$this->session->set_flashdata('status', 'success');
			$this->session->set_flashdata('messages', 'Login success');
			
			
			$data['is_login'] = 1;

			$this->session->set_userdata( $data );
			
			redirect(base_url());

		}
		else {

			$this->session->set_flashdata('status', 'warning');
			$this->session->set_flashdata('errors', $this->form_validation->error_array());

			redirect($this->agent->referrer());

		}	

	}

	public function category( $cat_slug )
	{

	}

	public function single( $slug )
	{
		$this->get_header();
		$this->data['slug'] = $slug;
		$this->load->view($this->get_template_directory_uri() . '/template-parts/single', $this->data);
		$this->get_footer();
	}

	public function archive($year, $month, $day)
	{

	}

	public function signin()
	{
		
		$this->get_header();
		$this->load->view($this->get_template_directory_uri() . '/template-parts/SignIn', $this->data);
		$this->get_footer();

	}

	public function check_signin()
	{
		$rules = [
			[
				'label' => 'Username',
				'field' => 'username',
				'rules' => 'required'
			],
			[
				'label' => 'Password',
				'field' => 'password_user',
				'rules' => 'required'
			],
		];

		$run = $this->form_validation->set_rules($rules);

		if($run) {
			
			$data = [
				'username' => $this->input->post('username'),
				'password_user' => md5($this->input->post('password_user')),
			];

			$search = $this->db->select()->from('pengguna')
				->join('roles', 'roles.id_role=pengguna.id_role')
				->where('username', $this->input->post('username'))
				->where('password_user', md5($this->input->post('password')))
				->get();

			if($search->num_rows() > 0) {


				$userdata = $search->row();
				$userdata->is_login = 1;
				$this->session->set_userdata( (array)$userdata );

				$this->session->set_flashdata('status', 'success');
				$this->session->set_flashdata('messages', 'Login success');
				
				redirect(base_url());

			}

			else {
				$this->session->set_flashdata('status', 'warning');
				$this->session->set_flashdata('messages', 'Username/Passworrd not match');
				redirect($this->agent->referrer());
			}

		}
		else {

			$this->session->set_flashdata('status', 'warning');
			$this->session->set_flashdata('errors', $this->form_validation->error_array());

			redirect($this->agent->referrer());
			

		}
	}

	public function signup()
	{

		$this->get_header();
		$this->load->view($this->get_template_directory_uri() . '/template-parts/SignUp', $this->data);
		$this->get_footer();

	}

	public function qna() {
		$this->data['_pageTitle'] = "TanyaJawab";						
		$this->get_header();
		$this->load->view($this->get_template_directory_uri() . '/template-parts/qna', $this->data);
		$this->get_footer();
	}

	public function jawab() {
		$this->data['_pageTitle'] = "Jawab";								
		$this->get_header();
		$this->load->view($this->get_template_directory_uri() . '/template-parts/jawabpertanyaan', $this->data);
		$this->get_footer();
	}

	public function ask() {

		if(!$this->session->userdata('is_login')) {
			$this->session->set_flashdata('status', 'warning');
			$this->session->set_flashdata('messages', 'Harus login terlebih dahulu');
				
			redirect(base_url('welcome/qna'));		
		}


		$this->db->insert('faq', [
			'faq_ts' => $this->input->post('faq_ts'),
			'faq_title' => $this->input->post('faq_title'),
			'faq_age' => $this->input->post('faq_age'),
			'faq_content' => $this->input->post('faq_content'),
			'faq_status' => 'pending',
			'faq_answer' => '#',
			'faq_date' => date('Y-m-d')

		]);

		$this->session->set_flashdata('status', 'success');
		$this->session->set_flashdata('messages', 'Pertanyaan anda telah masuk dalam antrian');
		

		redirect(base_url('welcome/qna'));


	}

	public function search()
	{
		$this->get_header();
		$this->load->view($this->get_template_directory_uri() . '/template-parts/search', $this->data);
		$this->get_footer();
	}

	public function post()
	{
		$this->data['_pageTitle'] = "Blog";				
		$this->get_header();
		$this->load->view($this->get_template_directory_uri() . '/template-parts/post', $this->data);
		$this->get_footer();
	}

	public function kuliner($slug) {
		$this->get_header();
		$this->data['slug'] = $slug;
		$this->load->view($this->get_template_directory_uri() . '/template-parts/kuliner', $this->data);
		$this->get_footer();
	}

	public function list() {
		$this->get_header();
		$this->load->view($this->get_template_directory_uri() . '/template-parts/list-kuliner', $this->data);
		$this->get_footer();
	}

	public function contact() {
		$this->data['_pageTitle'] = "Contact";		
		$this->get_header();
		$this->load->view($this->get_template_directory_uri() . '/template-parts/contact', $this->data);
		$this->get_footer();
	}

	public function promo() {
		$this->data['_pageTitle'] = "Promo";				
		$this->get_header();
		$this->load->view($this->get_template_directory_uri() . '/template-parts/promo', $this->data);
		$this->get_footer();
	}

	public function penjual() {
		$this->data['_pageTitle'] = "PenjualFav";
		$this->get_header();
		$this->load->view($this->get_template_directory_uri() . '/template-parts/penjual', $this->data);
		$this->get_footer();
	}

	public function logout() {
		
		$this->session->sess_destroy();
		redirect(base_url());
		
	}

	// buat perintah CRUD dari backend
	// codeingiter ini pakai Konsep HMVC jadi controller sama method nya jadi sistem routing
	
	// jadi di codeigniter cara buat controllernya 
	// itu buat class dengan nama filenya sama tapi hutuf awal harus huruf kapital
	// buat file nya di folder application/controllers

}

//Tugas Web 