<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {


	public function index()
	{
		$data['_pageTitle'] = "Contact";
		$this->load->view('contact/Contact',$data);
    }

}
