<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Taxonomy
{
    protected $ci;
    
    private $taxonomy = [];
    private $id = "";


    public function __construct()
    {
        $this->ci =& get_instance();
    }

    public function post_type( $name ) {
        $this->taxonomy['post_type'] = $name;
        return $this;
    }

    public function register($slug, $name) {
        $this->id = $slug;
        $this->taxonomy[$slug] = [
            'slug' => $slug,
            'name' => $name,
        ];

        return $this;
    }

    public function save($id_post, $data = []) {
        
        if(count($data) > 0 ) 
        {
            $this->ci->db->where('id_post', $id_post);
            $this->ci->db->delete('post_taxonomy');

            foreach($data as $d) {
                $this->ci->db->insert('post_taxonomy', [
                    'id_post' => $id_post,
                    'id_cat' => $d
                ]);
            }


        }

        return $this;

    }

    public function selected_taxs($taxs = []) {
        $this->taxonomy['selected'] = $taxs;
        return $this;
    }

    public function form() { 
        
        $this->ci->db
            ->select()
            ->from('categories')
            ->where('cat_type', $this->taxonomy[$this->id]['slug']);

        $taxs = $this->ci->db->get(); ?>

        <div class="form-group">
            <label for=""><?php echo ucfirst($this->taxonomy[$this->id]['name']); ?></label>
            <?php if($taxs->num_rows() > 0 ) :  ?>
          
                <?php foreach($taxs->result() as $tax ) : ?>
                    <div class="form-check">
                        <label class="form-check-label">
                        <input 
                            type="checkbox" 
                            class="form-check-input" 
                            name="id_cat[]" 
                            id="id_cat-<?php echo $tax->id_cat ?>" 
                            value="<?php echo $tax->id_cat ?>" 
                            <?php if(in_array($tax->id_cat, $this->taxonomy['selected'])) echo 'checked'; ?>>
                        <?php echo $tax->cat_name; ?>
                        </label>
                    </div>
                <?php endforeach ?>
            <?php endif ?>

        </div>
        


    <?php }



}

/* End of file Taxonomy.php */
