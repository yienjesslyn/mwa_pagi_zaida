
  <div class="breadcumb-area bg-img bg-overlay" style="background-image: url(<?= base_url() ?>assets/img/bg-img/bubur.jpg);">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcumb-text text-center">
            <h2>Tentang Kami dan Hubungi Kami</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ##### Breadcumb Area End ##### -->
  <!-- ##### About Area Start ##### -->
  <section class="about-area section-padding-80">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="section-heading">
            <h3 style="color: green;">APLIKASI INI KEDAN</h3>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <h6 class="sub-heading pb-5">INI KEDAN ( Informasi Gizi Kuliner Medan) merupakan sebuah aplikasi web yang bertujuan untuk menampilkan informasi gizi kuliner Medan sekaligus menawarkan tempat-tempat menarik yang menjual masing masing kuliner itu. Kata KEDAN di Medan dikenal luas dengan arti "Teman Akrab / Kawan Baik" yang berarti aplikasi INI KEDAN bisa menjadi teman baik para pengguna untuk bisa hidup sehat.</h6>
          <h6 class="sub-heading pb-5">Ayo Segera Temukan Fakta Menarik dibalik kuliner Medan yang lezaat ini !!!</h6>

          <p class="text-center mb-10 font-weight-bold">Medan merupakan salah satu kota kuliner terbaik di Indonesia (SumutCyber.com, 2019). Sayangnya, pengetahuan masyarakat mengenai ragam kuliner Kota Medan apalagi nilai gizi yang terkandung di dalamnya masih rendah. Hal ini dibuktikan dari angka prevalensi stunting (gizi buruk) balita Indonesia yang berada di angka 30,8%, turun 6,4% dari tahun 2013 (Kemenkes, 2018). Hasil Riskesdas 2018 juga menyebutkan kondisi konsumsi makanan ibu hamil dan balita tahun 2016-2017 menunjukkan di Indonesia 1 dari 5 ibu hamil kurang gizi, 7 dari 10 ibu hamil kurang kalori dan protein, 7 dari 10 balita kurang kalori, serta 5 dari 10 balita kurang protein (Kemenkes, 2018). Jadi, gizi sangat penting, terutama untuk orang dengan keadaan khusus, yaitu : balita, ibu hamil, hingga lansia. Menurut Prof. Astawan, penyebab utamanya adalah faktor ekonomi dan pendidikan gizi. Tidak sedikit masyarakat yang berkecukupan secara finansial memiliki pengetahuan gizi rendah, begitupun masyarakat yang memang kurang secara finansial, gizinya pun kurang (Kompas, 2013). Dalam konsep gizi, tak ada satupun makanan yang bisa mencakup semua gizi yang diperlukan tubuh. Sifat dari bahan pangan mengandung kelebihan dan kekurangan, ada yang tinggi dan rendah, kecuali air susu ibu untuk bayi di bawah 6 bulan (ZywieLab, 2016). </p>
          <br>
          <br>

          <!-- FITUR  -->
          <div class="row">
            <div class="col-12 col-lg-8">
              <div class="section-heading">
                <h3 style="color: green;">FITUR MENARIK INI KEDAN</h3>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-lg-8">
              <!-- Single Preparation Step -->
              <div class="single-preparation-step d-flex mb-3">
                <h5 class="mr-4" style="color: green;">01. </h5>
                <h6> Menyediakan informasi tentang kandungan gizi dari kuliner Kota Medan </h6>
              </div>
              <div class="single-preparation-step d-flex mb-3">
                <h5 class="mr-4" style="color: green;">02. </h5>
                <h6> Menampilkan perbandingan gizi kuliner dengan AKG (Angka Kecukupan Gizi) pengguna berdasarkan usia dan jenis kelamin </h6>
              </div>
              <div class="single-preparation-step d-flex mb-3">
                <h5 class="mr-4" style="color: green;">03. </h5>
                <h6> Memberikan peringatan bagi kuliner yang tidak cocok dengan ibu hamil, anak-anak, ataupun lansia</h6>
              </div>
              <div class="single-preparation-step d-flex mb-3">
                <h5 class="mr-4" style="color: green;">04. </h5>
                <h6> Menyediakan detail penjual kuliner beserta navigasi menuju lokasi kuliner</h6>
              </div>
              <div class="single-preparation-step d-flex mb-3">
                <h5 class="mr-4" style="color: green;">05. </h5>
                <h6> Menyediakan artikel tentang fakta menarik seputar pentingnya gizi dan hidup sehat </h6>
              </div>
              <div class="single-preparation-step d-flex mb-3">
                <h5 class="mr-4" style="color: green;">06. </h5>
                <h6> Menyediakan halaman untuk tanya jawab dengan dokter gizi</h6>
              </div>
            </div>
          </div>

          <br>
          <br>

          <!-- KEGUNAAN -->
          <div class="row">
            <div class="col-12 col-lg-4"></div>
            <div class="col-12 col-lg-8">
              <div class="section-heading">
                <h3 style="color: green;">KEGUNAAN APLIKASI INI KEDAN</h3>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-lg-4"></div>
            <div class="col-12 col-lg-8">
              <!-- Single Preparation Step -->
              <div class="single-preparation-step d-flex mb-3">
                <h5 class="mr-4" style="color: green;">01. </h5>
                <h6> Meningkatkan kesadaran masyarakat khususnya Kota Medan dan sekitarnya tentang pentingnya pemenuhan dan keseimbangan gizi </h6>
              </div>
              <div class="single-preparation-step d-flex mb-3">
                <h5 class="mr-4" style="color: green;">02. </h5>
                <h6> Menyediakan informasi kandungan nilai gizi dari kuliner Medan. </h6>
              </div>
              <div class="single-preparation-step d-flex mb-3">
                <h5 class="mr-4" style="color: green;">03. </h5>
                <h6>Memberikan informasi mengenai letak penjual kuliner Medan berdasarkan kategori makanan yang diinginkan.</h6>
              </div>
              <div class="single-preparation-step d-flex mb-3">
                <h5 class="mr-4" style="color: green;">04. </h5>
                <h6>Memberi infomasi mengenai fakta seputar gizi.</h6>
              </div>
              <h6> Dengan adanya aplikasi web ini diharapkan pengetahuan dan kepedulian masyarakat Indonesia akan gizi bertambah sehingga masalah gizi buruk balita, kekurangan gizi pada ibu hamil di Kota Medan dan sekitarnya bisa diminimalisir. </h6>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- ##### About Area End ##### -->

  <!-- ##### Contact Information Area Start ##### -->
  <div class="contact-information-area section-padding-80">
    <div class="container">
      <div class="row">
        <!-- Contact Text -->
        <div class="col-12 col-lg-6">
          <div class="contact-text ">
            <p style="color:black;">Aplikasi ini terbuka bagi para penjaja kuliner Kota Medan. Silahkan daftarkan toko / stan Anda disini. Ayo buruan Daftar !!! Tinggal kirim Email, kirimkan lokasi stan Anda! Setelah kami, verifikasi, maka stan Anda akan terdaftar.
            </p>
            <p style="color:black;">Hai, jika kalian memiliki keluhan, kritik dan saran terkait isi konten atau halaman lainnya dalam website Ini Kedan,
              atau jika kalian ingin membuat pertanyaan terkait gizi atau makanan dan segala hal yang perlu jawaban dari admin, bisa dengan mengisi form dibawah ini agar kami dapat me-reply, terimakasih.</p>
          </div>
        </div>

        <div class="col-12 col-lg-6">
          <!-- Single Contact Information -->
          <div class="single-contact-information mb-30">
            <h6>Alamat :</h6>
            <p style="color:black;">Jl.Bridjend Hamid No.8<br>Medan, Kode Pos 30145</p>
          </div>
          <!-- Single Contact Information -->
          <div class="single-contact-information mb-30">
            <h6>Nomor Telepon:</h6>
            <p style="color:black;">+62 813 7051 0867<br>+62 61 4153408</p>
          </div>
          <div class="single-contact-information mb-30">
            <h6>Nomor Whatsapp:</h6>
            <p style="color:black;">+62 813 7051 0867</p>
          </div>
          <div class="single-contact-information mb-30">
            <h6>Line ID</h6>
            <p style="color:black;">inikedan</p>
          </div>
          <!-- Single Contact Information -->
          <div class="single-contact-information mb-30">
            <h6>Email:</h6>
            <p style="color:black;">inikedan@gmail.com</p>
          </div>
        </div>
        <div>
          <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d995.499229409112!2d98.6906777!3d3.5881812!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa9385aa9773fce68!2sSTMIK%20-%20STIE%20Mikroskil%2C%20Kampus%20A!5e0!3m2!1sid!2sid!4v1576478956958!5m2!1sid!2sid" width="350" height="250" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>

        <!-- ##### Contact Information Area End ##### -->

        <!-- ##### Contact Form Area Start ##### -->
        <div class="contact-area section-padding-0-80">
          <div class="container">
            <div class="row">
              <div class="col-12">
                <div class="section-heading">
                  <h3>Hubungi Kami</h3>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-12">
                <div class="contact-form-area">
                  <form action="#" method="post">
                    <div class="row">
                      <div class="col-12 col-lg-6">
                        <input type="text" class="form-control" id="name" placeholder="Nama">
                      </div>
                      <div class="col-12 col-lg-6">
                        <input type="email" class="form-control" id="email" placeholder="E-mail">
                      </div>
                      <div class="col-12">
                        <input type="text" class="form-control" id="Subject" placeholder="Judul">
                      </div>
                      <div class="col-12">
                        <textarea name="message" class="form-control" id="Message" cols="30" rows="10" placeholder="Pesan"></textarea>
                      </div>
                      <div class="col-12 text-center">
                        <button class="btn delicious-btn mt-30" type="submit" onclick="kirim()">Kirim</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- ##### Contact Form Area End ##### -->

        <!-- ##### Google Maps ##### -->
        <div class="map-area">
          <div id="googleMap"></div>
        </div>


       

<script type="text/javascript">
  function kirim() {
    alert('Silahkan tunggu email konfirmasi dari Admin. Terima Kasih ...')
  }
</script>