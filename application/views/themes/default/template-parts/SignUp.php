    <!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="breadcrumb_iner">
              <div class="breadcrumb_iner_item">
                <p>Signup Page</p>
                <h2>Sign Up</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- breadcrumb start-->

    <!-- Form Element -->
    <div class="whole-wrap">
      <div class="container box-1170">
        <div class="section-top-border">
          <div class="row">
            <div class="col-lg-5 col-md-5 mr-10 mt-20">
              <div class="contact-form-area">
                <form action="<?php echo base_url('welcome/register'); ?>" method="POST">
                  <div class="mt-10">
                      <input type="text" name="username" placeholder="Username" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Username'" required class="form-control">
                  </div>
                  <div class="mt-10">
                      <input type="email" name="email" placeholder="Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email'" required class="form-control">
                  </div>
                  <div class="mt-10">
                      <input type="password" name="password_user" placeholder="Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'" required class="form-control">
                  </div>
                  <div class="mt-10">
                      <input type="password" name="re_password_user" placeholder="Re Type Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Re Type Password'" required class="form-control">
                  </div>

    
                  <div class="mt-10 mb-3">
                      <span class="mr-10">
                        <input  type="checkbox" name="agreement" id="Agree" value="1">
                      </span>
                      I agree with <a href="<?php echo base_url('page/terms-of-service') ?>"> Terms of service </a> in this website 
                  </div>
                  <div class="form-group mt-10">
                    <button type="submit" class="btn delicious-btn">Sign Up</button>
                  </div>
                </form>
              </div>
            </div>
        </div>
        </div> 
      </div>
      
    </div>
   
