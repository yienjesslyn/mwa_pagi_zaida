
  <?php 
  
    
    $foods = $this->db->select()->from('post')
        ->where('post_slug', $slug)
        ->get();

    $food = $foods->row();
  
  ?>
  <!-- ##### Blog Area Start ##### -->
  <div class="blog-area section-padding-20">
    <div class="container">
      <div class="row">
        <div class="col-5 col-md-5">
          <!-- Gambar  -->
          <div class="container float-left">
            <div class="row">
              <div class="col-12">
                <div class="receipe-slider owl-carousel">
                  <img src="<?php echo base_url('uploads/' . $food->post_thumbnail) ?>" alt="">
                </div>
              </div>
            </div>
          </div>
          <!-- Judul  -->
          <div class="receipe-headline my-5">
            <h2 class="ml-15"> <?php echo $food->post_title ?></h2>
            <h5 class="mb-30 ml-15">Terdaftar 80 penjaja di Medan</h5>
          </div>
        </div>

        <!-- Side Bar  -->
        <div class="col-7 col-lg-7 float-left">
          <div class="blog-sidebar-area ">
            <!-- Ringkasan Gizi  -->
            <h4 class="text-left mb-3">Ringkasan Gizi : </h4>
            <div class="col-12">
              <div class="delicious-cool-facts-area">
                <div class="row">
                  <!-- Milestone Gizi -->
                  <div class="col-12 col-sm-6 col-lg-3" style="height:130px;">
                    <div class="single-cool-fact">
                      <h3><span class="counter"><?php  echo $this->form->get_meta($food->id_post, 'kalori') ?></span></h3>
                      <h6>Kalori (kcal)</h6>
                    </div>
                  </div>
                  <!-- Milestone Gizi -->
                  <div class="col-12 col-sm-6 col-lg-3" style="height:130px;">
                    <div class="single-cool-fact">
                      <h3><span class="counter"><?php  echo $this->form->get_meta($food->id_post, 'lemak') ?></span></h3>
                      <h6>Lemak (g)</h6>
                    </div>
                  </div>
                  <!-- Milestone Gizi -->
                  <div class="col-12 col-sm-6 col-lg-3" style="height:130px;">
                    <div class="single-cool-fact">
                      <h3><span class="counter"><?php echo $this->form->get_meta($food->id_post, 'karbohidrat') ?></span></h3>
                      <h6>Karbohidrat (g)</h6>
                    </div>
                  </div>
                  <!-- Milestone Gizi -->
                  <div class="col-12 col-sm-6 col-lg-3" style="height:130px;">
                    <div class="single-cool-fact">
                      <h3><span class="counter"><?php  echo $this->form->get_meta($food->id_post, 'protein') ?></span></h3>
                      <h6>Protein (g)</h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <h4 class="text-left mb-15  mt-15">Perbandingan AKG : </h4>
            <!-- Select Box -->
            <form action="#" method="post">
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <select class="nice-select mb-15" id="sel1">
                      <option>Usia</option>
                      <option>10-12 tahun</option>
                      <option>13-15 tahun</option>
                      <option>16-19 tahun</option>
                      <option>20-45 tahun</option>
                      <option>45-59 tahun</option>
                      <option>> 60 tahun</option>
                    </select>
                  </div>
                </div>

                <div class="col-6">
                  <div class="form-group">
                    <select class="nice-select mb-15 " id="sel2">
                      <option>Jenis Kelamin</option>
                      <option>Laki-Laki</option>
                      <option>Perempuan</option>
                    </select>
                  </div>
                </div>
              </div>
            </form>

            <!-- AKG -->

            <div class="col-12">
              <!-- Loaders Area Start -->
              <div class="our-skills-area text-center">
                <div class="row">

                  <!-- Single-Bar circle AKG -->
                  <div class="col-12 col-sm-6 col-md-3" style="height:130px;">
                    <div class="single-pie-bar mb-80" data-percent="19">
                      <canvas class="bar-circle" width="70" height="70"></canvas>
                      <h6>Kalori</h6>
                    </div>
                  </div>
                  <!-- Single-Bar circle AKG -->
                  <div class="col-12 col-sm-6 col-md-3" style="height:130px;">
                    <div class="single-pie-bar mb-80" data-percent="38">
                      <canvas class="bar-circle" width="70" height="70"></canvas>
                      <h6>Lemak</h6>
                    </div>
                  </div>
                  <!-- Single-Bar circle AKG -->
                  <div class="col-12 col-sm-6 col-md-3" style="height:130px;">
                    <div class="single-pie-bar mb-80" data-percent="17">
                      <canvas class="bar-circle" width="70" height="70"></canvas>
                      <h6>Karbohidrat</h6>
                    </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-3" style="height:130px;">
                    <div class="single-pie-bar mb-80" data-percent="45">
                      <canvas class="bar-circle" width="70" height="70"></canvas>
                      <h6>Protein</h6>
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="alert alert-warning" role="alert">
              <strong>Kurang cocok untuk Balita</strong>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <hr>
  <!-- Bagian Penjual -->
  <div class="blog-area section-padding-20">
    <div class="container">
      <div class="row">
        <h2>Daftar Penjual</h2>

        <!-- ##### Small Receipe Area Start ##### -->
        <section class="small-receipe-area section-padding-80-0">
          <div class="container">
            <div class="row">

              <!-- Small Receipe Area -->
              <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-small-receipe-area d-flex">
                  <!-- Receipe Thumb -->
                  <div class="receipe-thumb">
                    <img src="<?php echo base_url('assets/img/bg-img/afrizal.jpg') ?>" alt="">
                  </div>
                  <!-- Receipe Content -->
                  <div class="receipe-content">
                    <span>January 04, 2018</span>
                    <a href="<?php echo base_url('index.php/detailpenjual') ?>">
                      <h5>Sate Padang Afrizal Amir</h5>
                    </a>
                    <div class="ratings">
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star-o" aria-hidden="true"></i>
                    </div>
                    <p>2 Comments</p>
                  </div>
                </div>
              </div>

              <!-- Small Receipe Area -->
              <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-small-receipe-area d-flex">
                  <!-- Receipe Thumb -->
                  <div class="receipe-thumb">
                    <img src="<?php echo base_url('assets/img/bg-img/paris.jpg') ?>" alt="">
                  </div>
                  <!-- Receipe Content -->
                  <div class="receipe-content">
                    <span>January 04, 2018</span>
                    <a href="receipe-post.html">
                      <h5>Sate Padang Paris Bang Man</h5>
                    </a>
                    <div class="ratings">
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star-o" aria-hidden="true"></i>
                    </div>
                    <p>2 Comments</p>
                  </div>
                </div>
              </div>

              <!-- Small Receipe Area -->
              <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-small-receipe-area d-flex">
                  <!-- Receipe Thumb -->
                  <div class="receipe-thumb">
                    <img src="<?php echo base_url('assets/img/bg-img/waspada.jpg') ?>" alt="">
                  </div>
                  <!-- Receipe Content -->
                  <div class="receipe-content">
                    <span>January 04, 2018</span>
                    <a href="receipe-post.html">
                      <h5>Sate Padang Simpang Waspada</h5>
                    </a>
                    <div class="ratings">
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star-o" aria-hidden="true"></i>
                    </div>
                    <p>2 Comments</p>
                  </div>
                </div>
              </div>

              <!-- Small Receipe Area -->
              <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-small-receipe-area d-flex">
                  <!-- Receipe Thumb -->
                  <div class="receipe-thumb">
                    <img src="<?php echo base_url('assets/img/bg-img/joandah.jpg') ?>" alt="">
                  </div>
                  <!-- Receipe Content -->
                  <div class="receipe-content">
                    <span>January 04, 2018</span>
                    <a href="receipe-post.html">
                      <h5>Sate Padang Jo Andah</h5>
                    </a>
                    <div class="ratings">
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star-o" aria-hidden="true"></i>
                    </div>
                    <p>2 Comments</p>
                  </div>
                </div>
              </div>

              <!-- Small Receipe Area -->
              <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-small-receipe-area d-flex">
                  <!-- Receipe Thumb -->
                  <div class="receipe-thumb">
                    <img src="<?php echo base_url('assets/img/bg-img/alfresco.jpg') ?>" alt="">
                  </div>
                  <!-- Receipe Content -->
                  <div class="receipe-content">
                    <span>January 04, 2018</span>
                    <a href="receipe-post.html">
                      <h5>Sate Padang Al'fresco</h5>
                    </a>
                    <div class="ratings">
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star-o" aria-hidden="true"></i>
                    </div>
                    <p>2 Comments</p>
                  </div>
                </div>
              </div>

              <!-- Small Receipe Area -->
              <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-small-receipe-area d-flex">
                  <!-- Receipe Thumb -->
                  <div class="receipe-thumb">
                    <img src="<?php echo base_url('assets/img/bg-img/afrizal.jpg') ?>" alt="">
                  </div>
                  <!-- Receipe Content -->
                  <div class="receipe-content">
                    <span>January 04, 2018</span>
                    <a href="receipe-post.html">
                      <h5>Sate Padang H. Amiruddin (Bata)</h5>
                    </a>
                    <div class="ratings">
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star-o" aria-hidden="true"></i>
                    </div>
                    <p>2 Comments</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- ##### Small Receipe Area End ##### -->
      </div>
    </div>
  </div>
  <!-- ##### Blog Area End ##### -->
  <!-- Page Navigation -->

  <div class="justify-content-center mb-15">
    <nav aria-label="Page navigation">
      <ul class="pagination justify-content-center mb-15">
        <li class="page-item disabled">
          <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
          </a>
        </li>
        <nav aria-label="Page navigation example">
          <ul class="pagination">
            <li class="page-item active"><a class="page-link" href="#">01.</a></li>
            <li class="page-item"><a class="page-link" href="#">02.</a></li>
            <li class="page-item"><a class="page-link" href="#">03.</a></li>
          </ul>
        </nav>
        <li class="page-item active"><a class="page-link" href="#"></a></li>
        <li class="page-item"><a class="page-link" href="#"></a></li>
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
          </a>
        </li>
      </ul>
    </nav>
  </div>


