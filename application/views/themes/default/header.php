<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<!-- Title -->
	<title>INI KEDAN | Kuliner Medan</title>

	<!-- Favicon -->
	<link rel="icon" href="img/core-img/favicon.ico">

	<!-- Core Stylesheet -->
	<link rel="stylesheet" href="<?php echo base_url('assets/style.css') ?>">

	<script src="<?php echo base_url('plugins/tinymce/js/tinymce/tinymce.min.js') ?>"></script>
	<script>
		tinymce.init({
			selector: '#editor'
		});
	</script>
</head>

<body>
	<!-- Preloader -->
	<div id="preloader">
		<i class="circle-preloader"></i>
		<img src="<?php echo base_url('assets/img/core-img/salad.png') ?>" alt="">
	</div>

	<!-- Search Wrapper -->
	<div class="search-wrapper">
		<!-- Close Btn -->
		<div class="close-btn">
			<i class="fa fa-times" aria-hidden="true"></i>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-12">
					<form action="<?php echo base_url('/search') ?>" method="get">
						<input type="hidden" name="post_type" value="kuliner">
						<input type="search" name="s" placeholder="Type any keywords...">
						<button type="submit">
							<i class="fa fa-search" aria-hidden="true"></i>
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- ##### Header Area Start ##### -->
	<header class="header-area">

		<!-- Top Header Area -->
		<div class="top-header-area">
			<div class="container h-100">
				<div class="row h-100 align-items-center justify-content-between">
					<!-- Breaking News -->
					<div class="col-12 col-sm-6">
						<div class="breaking-news">
							<!-- SignIn -->

							<?php $is_login = $this->session->userdata('is_login'); ?>


							<?php if(!$is_login) : ?>
							<a href="<?php echo base_url('signin') ?>" class="mr-4" style ="font-size: 14pt">Sign in</a>
							<a href="<?php echo base_url('signup') ?>" style ="font-size: 14pt" >Sign up</a>
							<?php else: ?>

							<a href="<?php echo base_url('welcome/logout') ?>">Logout</a>

							<?php endif; ?>
						</div>
					</div>

					<!-- Top Social Info -->
					<div class="col-12 col-sm-6">
						<div class="top-social-info text-right">
							<div class="login-btn">
							</div>
							<div class="search-btn">
								<i class="fa fa-search" aria-hidden="true"></i>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<!-- Navbar Area -->
		<div class="delicious-main-menu">
			<div class="classy-nav-container breakpoint-off">
				<div class="container">
					<!-- Menu -->
					<nav class="classy-navbar justify-content-between" id="deliciousNav">

						<!-- Logo -->
						<a class="nav-brand" href="<?php echo base_url('/') ?>">
							<img src="<?php echo base_url('assets/img/core-img/nvidia.png') ?>" alt="">
						</a>
						<!-- Navbar Toggler -->
						<div class="classy-navbar-toggler">
							<span class="navbarToggler">
								<span></span>
								<span></span>
								<span></span>
							</span>
						</div>

						<!-- Menu -->
						<div class="classy-menu">

							<!-- close btn -->
							<div class="classycloseIcon">
								<div class="cross-wrap">
									<span class="top"></span>
									<span class="bottom"></span>
								</div>
							</div>

							<!-- Nav Start -->
							<div id="navigasi justify-content-center" class="classynav">
								<ul>
									<li class="nav" id="homeLink">
										<a href="<?php echo base_url('/') ?>">Beranda</a>
									</li>
									<li class="nav" id="blogLink">
										<a href="<?php echo base_url('post') ?>">Artikel</a>
									</li>
									<li id="categoryLink">
										<a href="#">Kategori</a>
										<ul class="dropdown">
											<li>
												<a onclick="semua()" href="<?php echo base_url('makanan') ?>">Semua</a>
											</li>
											<li>
												<a href="#">Jenis</a>
												<ul class="dropdown">
													<li>
														<a href="<?php echo base_url('index.php/makanan') ?>">Makanan</a>
													</li>
													<li>
														<a href="<?php echo base_url('index.php/minuman') ?>">Minuman</a>
													</li>
												</ul>
											</li>
											<li>
												<a href="#">Fitur</a>
												<ul class="dropdown">
													<li>
														<a href="<?php echo base_url('index.php/halal') ?>">Halal</a>
													</li>
													<li>
														<a href="<?php echo base_url('index.php/nonhalal') ?>">Non-Halal</a>
													</li>
													<li>
														<a href="<?php echo base_url('index.php/vege') ?>">Vegetarian</a>
													</li>
												</ul>
											</li>
										</ul>
									</li>
									<li class="nav" id="AskLink">
										<a href="<?php echo base_url('ask'); ?>">Tanya Ahli Gizi</a>
									</li>

									<li class="nav" id="Jawab">
										<a href="<?php echo base_url('jawab'); ?>">Buat Artikel</a>
									</li>

									<li class="nav" id="FavLink">
										<a href="<?php echo base_url('penjual'); ?>"> Favorit</a>
									</li>
									</li>
									<li class="nav" id="Promolink">
										<a href="<?php echo base_url('promo'); ?>">Promo</a>
									</li>
									</li>
									<li class="nav" id="contactLink">
										<a href="<?php echo base_url('contact'); ?>">Contact</a>
									</li>
									</li>
									<li>
										<div class="search-btn">
										</div>
									</li>
								</ul>
							</div>
							<!-- Nav End -->
						</div>
					</nav>
				</div>
			</div>
		</div>
	</header>
	<!-- ##### Header Area End ##### -->

	<script type="text/javascript">
		// Aktifkan Link 
		var pageTitle = "<?php echo $_pageTitle ?>"
    
		if (pageTitle == "Home") {
			document.getElementById('homeLink').classList.add('active')
		} else if (pageTitle == "About") {
			document.getElementById('aboutLink').classList.add('active')
		} else if (pageTitle == "Blog") {
			document.getElementById('blogLink').classList.add('active')
		} else if (pageTitle == "Contact") {
			document.getElementById('contactLink').classList.add('active')
		} else if (pageTitle == "DaftarKuliner") {
			document.getElementById('categoryLink').classList.add('active')
		} else if (pageTitle == "DetailGizi") {
			document.getElementById('categoryLink').classList.add('active')
		} else if (pageTitle == "DetailPenjual") {
			document.getElementById('categoryLink').classList.add('active')
		} else if (pageTitle == "Artikel") {
			document.getElementById('blogLink').classList.add('active')
		} else if (pageTitle == "Artikel2") {
			document.getElementById('blogLink').classList.add('active')
		} else if (pageTitle == "PenjualFav") {
			document.getElementById('FavLink').classList.add('active')
		} else if (pageTitle == "TanyaJawab") {
			document.getElementById('AskLink').classList.add('active')
		} else if (pageTitle == "Promo") {
			document.getElementById('Promolink').classList.add('active')
		} else if (pageTitle == "Jawab") {
			document.getElementById('Jawab').classList.add('active')
		}
	</script>