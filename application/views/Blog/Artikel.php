<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("_Partials/Head.php") ?>
</head>

<body>
  <!-- Preloader -->
  <?php $this->load->view("_Partials/Preloader.php") ?>

  <!-- Search Wrapper -->
  <?php $this->load->view("_Partials/SearchWrapper.php") ?>

  <!-- ##### Header Area Start ##### -->
  <?php $this->load->view("_Partials/HeaderArea.php") ?>
  <!-- ##### Header Area End ##### -->

  <!-- ##### Breadcumb Area Start ##### -->
  <div class="breadcumb-area bg-img bg-overlay" style="background-image: url(<?= base_url() ?>assets/img/bg-img/sate_memeng.jpg);">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcumb-text text-center">
            <h2>Artikel</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ##### Breadcumb Area End ##### -->

  <!-- ##### Blog Area Start ##### -->
  <div class="blog-area section-padding-80">
    <div class="container">
      <div class="row">
        <div class="col-8 col-md-8">
          <!-- Judul  -->
          <div class="receipe-headline">
            <span>11 Desember 2019</span>
            <h3>Rutin Jalan Kaki Saat Hamil, Bunda Akan Merasakan 5 Manfaat ini </h3>
            <div class="receipe-duration">
              <blockquote><i>Dok, Apa iya rutinitas jalan kaki saat hamil juga perlu disamping makan makanan bergizi bagi bayi ? Berapa lama durasi jalan kaki yang dianjurkan per hari? </i></blockquote>
              <i>~ Rose, 29 tahun</i>
            </div>
          </div>

          <!-- Single Preparation Step -->


          <p>
            Jalan kaki saat hamil mungkin tak terpikirkan untuk dilakukan oleh Bunda karena sudah merasa kelelahan membawa bayi dalam kandungan. Padahal, jalan kaki amat direkomendasikan untuk dilakukan selama kehamilan dan baik bagi janin dalam kandungan.</p>

          <p>Tak perlu lama-lama, cukup lakukan 30 menit setiap hari. Anda bisa berjalan kaki di taman dekat rumah atau mengitari komplek perumahan. Jangan lupa ajak pak suami untuk olahraga bersama, ya, Bun!</p>

          <h5>Manfaat jalan kaki saat hamil #1: Mengurangi risiko diabetes</h5>
          <p>Kadar gula darah umumnya akan meningkat selama kehamilan, apalagi untuk Bunda yang ngidam makanan manis selama hamil. Jalan kaki saat hamil bisa mengurangi risiko penyakit ini.</p>

          <p>Tak hanya risiko diabetes, berjalan kaki ketika hamil akan membantu menjaga berat badan Bunda dan janin dalam kandungan lahir dengan berat badan normal.</p>

          <h5>Manfaat jalan kaki saat hamil #2: Meminimalkan risiko preeklamsia</h5>

          <!-- Gambar  -->
          <div class="container">
            <div class="row">
              <div class="col-12">
                <div class="receipe-slider owl-carousel">
                  <img src="<?php echo base_url('assets/img/bg-img/jalankaki.jpg') ?>" alt="">
                  <img src="<?php echo base_url('assets/img/bg-img/bg5.jpg') ?>" alt="">
                </div>
              </div>
            </div>
          </div>

          <p>Preeklamsia biasanya ditandai tekanan darah yang tinggi dan adanya protein dalam urin, dan dengan jalan kaki risiko ini bisa dikurangi risikonya.</p>

          <p> kaki saat hamil akan membantu menjaga berat badan bunda tetap ideal, mengurangi kadar kolesterol sehingga kesehatan kehamilan tetap terjaga.</p>

          <h5>Manfaat jalan kaki saat hamil #3: Memudahkan proses persalinan</h5>
          <p>Jalan kaki saat hamil juga akan memudahkan Bunda melalui proses persalinan, apalagi jika Anda ingin lahiran normal (vaginal birth). Dengan berjalan kaki, otot akan semakin kuat setiap waktu, sehingga persalinan akan terasa cepat dan mudah.</p>

          <h5>Manfaat jalan kaki saat hamil #4: Mengurangi stres</h5>

          <p>Saat hamil merupakan hal yang wajar terjadi akibat perubahan hormon yang memengaruhi suasana hati. Mulai dari bahagia hingga tiba-tiba berubah menjadi gelisah bahkan depresi.</p>

          <p>Jalan kaki saat hamil akan memproduksi hormon endorfin, hormon yang akan mengubah mood Bunda menjadi lebih baik sepanjang kehamilan.</p>

          <h5>Manfaat jalan kaki saat hamil #5: Efektif memperbaiki kualitas tidur</h5>
          <p> Muntah, nyeri merupakan sederet rasa tidak nyaman yang diibaratkan menjadi sahabat Anda kala menjalani kehamilan. Nah, dengan jalan kaki, ketidaknyamanan yang dirasakan akan perlahan berkurang.</p>

          <p>Berjalan santai di pagi hari akan mengurangi keluhan-keluhan yang telah disebutkan. Selain itu, rutin berjalan kaki ampuh membuat Bunda bisa tidur dengan nyenyak saat malam hari.</p>

          <h5> lama jalan kaki yang dianjurkan selama hamil?</h5>
          <p>Menurut the Physical Activity Guidelines for Americans dan NHS, durasi jalan kaki saat hamil yang dianjurkan yaitu 150 menit dalam seminggu. Bunda bisa berjalan kaki 45-60 menit pada dua trimester pertama lalu 30 menit saat kehamilan sudah memasuki trimester ketiga.</p>

          <p> lakukan sesuai kemampuan Anda ya, Bun. Jangan langsung paksakan untuk melakukannya setiap hari, tetapi coba bertahap saja. Kenakan alas kaki yang nyaman dan jangan lupa selalu membawa air putih agar tubuh Anda selalu terhidrasi.
          </p>

          <h5> oleh : dr.Giasinta</h5>

          <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2"><i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>

        </div>
        <div class="col-4 col-lg-4">
          <div class="blog-sidebar-area">

            <!-- Widget -->
            <div class="single-widget mb-80">
              <h6>Arsip</h6>
              <ul class="list">
                <li><a href="#">Maret 2018</a></li>
                <li><a href="#">Februari 2018</a></li>
                <li><a href="#">Januari 2018</a></li>
                <li><a href="#">Desember 2017</a></li>
                <li><a href="#">November 2017</a></li>
              </ul>
            </div>

            <!-- Widget -->
            <div class="single-widget mb-80">
              <h6>Kategori</h6>
              <ul class="list">
                <li><a href="#">Kehamilan</a></li>
                <li><a href="#">Gizi Anak</a></li>
                <li><a href="#">Kekurangan Gizi</a></li>
                <li><a href="#">Belum Terkategori</a></li>
              </ul>
            </div>


            <!-- Widget -->
            <div class="single-widget mb-80">
              <div class="quote-area text-center">
                <span>"</span>
                <h4>Nothing is better than going home to family and eating good food and relaxing</h4>
                <p>John Smith</p>
                <div class="date-comments d-flex justify-content-between">
                  <div class="date">January 04, 2018</div>
                  <div class="comments">2 Komentar</div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>


    </div>
  </div>
  <!-- ##### Blog Area End ##### -->

  <section class="quote-subscribe-addsn mt-5">
    <div class="container">
      <div class="row align-items-end">
        <!-- ##### Follow Us Instagram Area Start ##### -->
        <?php $this->load->view("_Partials/GaleriEnd.php") ?>
        <!-- ##### Follow Us Instagram Area End ##### -->

        <!-- ##### Footer Area Start ##### -->
        <?php $this->load->view("_Partials/Footer.php") ?>
        <!-- ##### Footer Area Start ##### -->

      </div>
    </div>
  </section>
  <!-- ##### All Javascript Files ##### -->
  <?php $this->load->view("_Partials/Js.php") ?>
</body>

</html>