<?php

// INI DATA DARI DATABASE, SESUAIKAN NNTI YA.
// LINK KE SECTION FORM
$id_post      = isset($row->id_post) ? $row->id_post : false;
$post_title   = isset($row->post_title) ? $row->post_title : false;
$post_slug    = isset($row->post_slug) ? $row->post_slug : false;
$post_content = isset($row->post_content) ? $row->post_content : false;
$post_author  = isset($row->post_author) ? $row->post_author : false;
$post_type    = isset($row->post_type) ? $row->post_type : $this->input->get('post_type');
$post_thumbnail = isset($row->post_thumbnail) ? $row->post_thumbnail : false;

// REGISTERD POST TYPE
$registered_post_type = $register['post_type'][$post_type]; 
$title_page = $registered_post_type['title'];




$post_taxs = $this->db->select('id_cat')
  ->from('post_taxonomy')
  ->where('id_post', $id_post)
  ->get();

$selected_taxs = [];
if($post_taxs->num_rows() > 0 ) {
  foreach($post_taxs->result() as $tax) {
    $selected_taxs[] = $tax->id_cat;
  }
}

//var_dump($selected_taxs);


?>
<div class="container-fluid">
  <div class="row" id="mastah-post-row">
  

    <div class="col-12 col-md-4 mastah-form-col mb-3">

      <?php if ($this->session->flashdata('status')) : ?>

        <div class="alert alert-<?php echo $this->session->flashdata('status') ?> alert-dismissible fade show mb-2" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          <?php echo $this->session->flashdata('message') ?>
        </div>

      <?php endif; ?>

      <div class="d-flex align-items-center">
        
        <h3 class="mr-2"><?php echo $title_page; ?></h3>
        <div>
          <a href="javascript:void(0)" class="minimize-table">Fullsize</a>
          <a href="javascript:void(0)" class="reverse-table">Reverse</a>
        </div>
      </div>
      <hr>
      <!-- SECTION FORM -->
      <!-- INI INPUT YANG HARUS DI SESUAIKAN DARI DATABASE -->
      <!-- BUAT AJA "INPUT NAME" NYA NGIKUTIN NAMA FIELD DI DATABASE -->
      <!-- $SUBMIT URL ITU  ADA DI CONTROLLER DI METHOD INDEX  -->
      <form method="POST" action="<?php echo base_url($submit_url); ?>" enctype="multipart/form-data">
        <input type="hidden" name="post_type" value="<?php echo $post_type; ?>">
        <input type="hidden" name="post_author" value="1">
        <input type="hidden" name="taxonomy" value="<?php echo $this->input->get('taxonomy'); ?>">
        <input type="hidden" name="taxonomy_type" value="<?php echo $this->input->get('taxonomy_type'); ?>">
        <!-- KHUSUS INI NAMENYA JANGAN DI GANTI BIAR KAN PAKAI "id" aja VALUES nya aja diganti -->
        <input type="hidden" name="id" value="<?php echo $id_post ?>">

        <!-- YANG INI DIGANTI SEMUA -->

        <div class="form-group">
          <label for="i-post_title">Title</label>
          <input type="text" class="form-control" name="post_title" id="i-post_title" value="<?php echo $post_title ?>">
        </div>

        <div class="form-group">
          <label for="i-post_content">Content</label>
          <textarea name="post_content" id="editor" rows="10" class="form-control"><?php echo $post_content ?></textarea>
        </div>

        <?php 

          $fullpath_thumbnail = "";
          if($post_thumbnail) $fullpath_thumbnail = base_url('uploads/' . $post_thumbnail);

          if($post_type == 'post')
          {

            $this->form->image('post_thumbnail', 'Thumbnail', $post_thumbnail, $fullpath_thumbnail);


            $this->taxonomy->register('category', 'Category');
            $this->taxonomy->selected_taxs($selected_taxs)->form();

          }
          
          if($post_type == 'kuliner') 
          {

            $kalori = $this->form->get_meta($id_post, 'kalori');
            $lemak = $this->form->get_meta($id_post, 'lemak');
            $karbohidrat = $this->form->get_meta($id_post, 'karbohidrat');
            $protein = $this->form->get_meta($id_post, 'protein');
            $rating = $this->form->get_meta($id_post, 'rating');

            $this->form->text('meta[kalori]','Kalori', $kalori);
            $this->form->text('meta[lemak]','Lemak', $lemak);
            $this->form->text('meta[karbohidrat]','Karbohidrat', $karbohidrat);
            $this->form->text('meta[protein]','Protein', $protein);
            $this->form->text('meta[rating]','Rating', $rating);

            $this->form->image('post_thumbnail', 'Thumbnail', $post_thumbnail, $fullpath_thumbnail);


            $this->taxonomy->register('kuliner', 'Category Kuliner');
            $this->taxonomy->selected_taxs($selected_taxs)->form();

          }

          if($post_type == 'slider') 
          {
            
            
            
            $btn_cta_name = $this->form->get_meta($id_post, 'btn_cta_name');
            $btn_cta_url = $this->form->get_meta($id_post, 'btn_cta_url');
            $btn_cta_target = $this->form->get_meta($id_post, 'btn_cta_target');
            $this->form->text('meta[btn_cta_name]', 'Button CTA Name', $btn_cta_name);
            $this->form->text('meta[btn_cta_url]', 'Button CTA URI', $btn_cta_url);
            $this->form->select('meta[btn_cta_target]', 'Button CTA Target', [
              'Self' => '_self',
              'Blank' => '_blank'
            ], $btn_cta_target);
            $this->form->image('post_thumbnail', 'Thumbnail', $post_thumbnail, $fullpath_thumbnail);
          

            $this->taxonomy->register('slider', 'Slide');
            $this->taxonomy->selected_taxs($selected_taxs)->form(); 


            
          }
        
        ?>
        <!-- SAMPAI SINI -->

        <button type="submit" name="submit" class="btn btn-primary">Save</button>
      </form>
    </div>

    <div class="col-12 col-md-8 mastah-table-col">
      
      <table class="table table-bordered">
        
        
        <!-- TAMPILKAN NAMA FIELD YANG DIBUTUHKAN DARI DATABSE -->
        <thead>
          <tr>
            <th>ID</th>
            <th>Title</th>   

            <?php if($registered_post_type['taxonomy']) : ?>

              <th><?php echo $registered_post_type['taxonomy']['name'] ?></th>

            <?php endif ?>
            <th>Author</th>
          </tr>
        </thead>
        <tbody>

          <!-- SESUAI KAN SAMA DATABASE -->
          <?php if ($data->num_rows() > 0) : ?>
            <?php foreach ($data->result() as $d) : ?>
              <tr>
                <td><?php echo $d->id_post ?></td>
                <td>
                  <div><?php echo $d->post_title ?></div>
                  <div class="d-flex align-items-center">
                    <a class="mr-1" href="<?php echo base_url('dashboard/post/delete/?id=' . $d->id_post) ?>">Delete</a> 
                    <span class="mr-1">|</span>
                    <a class="" href="<?php echo base_url('dashboard/post/?id=' . $d->id_post . '&post_type=' . $post_type . '&taxonomy=' . $this->input->get('taxonomy') ) ?>">Edit</a>
                  </div>
                </td>      

                <?php if($registered_post_type['taxonomy']) : ?>          
                <td>
                  <?php 
                  
                  
                    $categories = $this->db->select()
                      ->from('post_taxonomy')
                      ->join('categories', 'categories.id_cat=post_taxonomy.id_cat', 'left')
                      ->where('categories.cat_type', $this->input->get('taxonomy'))
                      ->where('post_taxonomy.id_post', $d->id_post)
                      ->get();

                    $cat_name = [];
                    if($categories->num_rows() > 0) {
                      foreach($categories->result() as $c) {
                        $cat_name[] = $c->cat_name;
                      }

                      echo implode(', ', $cat_name);
                    }
                  
                  
                  ?>
                </td>

                <?php endif ?>

                <td><?php echo $d->post_author ?></td>

              </tr>
            <?php endforeach; ?>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>