<div class="container-fluid">
    <div class="row">

        <div class="col-12 col-md-4">


            <form action="<?php echo base_url('dashboard/menu/add') ?>" method="POST">

                <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" class="form-control" name="menu_name" value="">
                </div>

                <div class="form-group">
                    <label for="">Url</label>
                    <input type="text" class="form-control" name="menu_url" value="">
                </div>

                <div class="form-group">

                    <label for="">Parent</label>
                    <select name="menu_parent" id="i-menu_parent" class="form-control">
                        <option value="0">Parent</option>
                        <?php 
                        
                            $this->db->select()->from('menus')->where('menu_parent', 0);
                            $parents = $this->db->get(); ?>

                        <?php if($parents->num_rows() > 0 ) : ?>
                            <?php foreach($parents->result() as $parent) : var_dump($parent); ?>        
                                
                                <option value="<?php echo $parent->id_menu ?>"><?php echo $parent->menu_name; ?></option>
                                
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            
                <button class="btn btn-primary" name="add_menu">Add new menu</button>

            </form>

        </div>

        <div class="col-12 col-md-4">

            <form action="<?php echo base_url('dashboard/menu/update') ?>" method="POST">
               
                <?php 
                
                $parents = $this->db->select()->from("menus")->where('menu_parent', 0)->get();
                if($parents->num_rows() > 0 ) {
                    
                    echo '<button class="btn btn-primary mb-3" name="update_menu">Update Menu</button>';

                    foreach($parents->result() as $parent) { ?>
                        
                        
                        <div class="card mb-3" style="width: 18rem;">
                            <input type="hidden" name="id_menu[]" value="<?php echo $parent->id_menu; ?>">
                            <input type="hidden" name="menu_parent[<?php echo $parent->id_menu ?>]" value="<?php echo $parent->menu_parent; ?>">
                            <div class="card-header" data-toggle="collapse" data-target="#menu-<?php echo $parent->id_menu ?>">
                                <div class="d-flex justify-content-between">
                                    <span><?php echo $parent->menu_name; ?></span>
                                    <a href="javascript:void(0)" class="remove-menu">Remove</a>
                                </div>
                            </div>
                            <div class="card-body collapse" id="menu-<?php echo $parent->id_menu ?>">
                                <div class="form-group">
                                    <label for="">Name</label>
                                    <input type="text" class="form-control form-control-sm" name="menu_name[<?php echo $parent->id_menu ?>]" value="<?php echo $parent->menu_name; ?>">
                                </div>

                                <div class="form-group">
                                    <label for="">Url</label>
                                    <input type="text" class="form-control form-control-sm" name="menu_url[<?php echo $parent->id_menu ?>]" value="<?php echo $parent->menu_url; ?>">
                                </div>
                            </div>
                        </div>

                                

                        

                    <?php }

                }
                
                ?>

            </form>

        </div>

    </div>
</div>