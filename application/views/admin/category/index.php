<?php

// INI DATA DARI DATABASE, SESUAIKAN NNTI YA.
// LINK KE SECTION FORM
$id_cat = isset($row->id_cat) ? $row->id_cat : false;
$cat_slug = isset($row->cat_slug) ? $row->cat_slug : false;
$cat_name = isset($row->cat_name) ? $row->cat_name : false;
$cat_desc = isset($row->cat_desc) ? $row->cat_desc : false;


?>
<div class="container-fluid">
  <div class="row">
    <?php if ($this->session->flashdata('status')) : ?>

      <div class="col-12">
        <div class="alert alert-<?php echo $this->session->flashdata('status') ?> alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          <?php echo $this->session->flashdata('message') ?>
        </div>
      </div>

    <?php endif; ?>

    <div class="col-12 col-md-3">
      <h3>Category</h3>
      <hr>
      <!-- SECTION FORM -->
      <!-- INI INPUT YANG HARUS DI SESUAIKAN DARI DATABASE -->
      <!-- BUAT AJA "INPUT NAME" NYA NGIKUTIN NAMA FIELD DI DATABASE -->
      <!-- $SUBMIT URL ITU  ADA DI CONTROLLER DI METHOD INDEX  -->
      <form method="POST" action="<?php echo base_url($submit_url); ?>">

        <!-- KHUSUS INI NAMENYA JANGAN DI GANTI BIAR KAN PAKAI "id" aja VALUES nya aja diganti -->
        <input type="hidden" name="id" value="<?php echo $id_cat ?>">
        <input type="hidden" name="cat_type" value="<?php echo $this->input->get('taxonomy'); ?>">

        <!-- YANG INI DIGANTI SEMUA -->

        <div class="form-group">
          <label for="i-cat_name">Name</label>
          <input type="text" class="form-control" name="cat_name" id="i-cat_name" value="<?php echo $cat_name ?>">
        </div>

        <div class="form-group">
          <label for="i-cat_desc">Description</label>
          <textarea name="cat_desc" id="i-cat_desc" rows="5" class="form-control"><?php echo $cat_desc ?></textarea>
        </div>
        <!-- SAMPAI SINI -->

        <button type="submit" name="submit" class="btn btn-primary">Save</button>
      </form>
    </div>

    <div class="col-12 col-md-9">

      <table class="table table-bordered">

        <!-- TAMPILKAN NAMA FIELD YANG DIBUTUHKAN DARI DATABSE -->
        <thead>
          <tr>
            <th>ID</th>    
            <th>Name</th>
            <th>Desc</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>

          <!-- SESUAI KAN SAMA DATABASE -->
          <?php if ($data->num_rows() > 0) : ?>
            <?php foreach ($data->result() as $d) : ?>
              <tr>
                <td><?php echo $d->id_cat ?></td>         
                <td><?php echo $d->cat_name ?></td>
                <td><?php echo $d->cat_desc ?></td>
                <td>
                  <a class="btn btn-sm btn-danger" href="<?php echo base_url('dashboard/category/delete/?id=' . $d->id_cat) ?>">Delete</a>
                  <a class="btn btn-sm btn-warning" href="<?php echo base_url('dashboard/category/?id=' . $d->id_cat . '&taxonomy=' . $this->input->get('taxonomy')) ?>">Edit</a>
                </td>
              </tr>
            <?php endforeach; ?>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>