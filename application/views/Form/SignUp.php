<!DOCTYPE html>
<html lang="en">
  <head>
    <?php $this->load->view("_Partials/Head.php") ?>
  </head>

  <body>
    <!-- Header part start-->
    <header>
     <?php $this->load->view("_Partials/Header.php") ?>
    </header>
    <!-- Header part end-->

    <!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="breadcrumb_iner">
              <div class="breadcrumb_iner_item">
                <p>Halaman Pendaftaran</p>
                <h2>Daftarkan Diri Anda</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- breadcrumb start-->

    <!-- Form Element -->
    <div class="whole-wrap">
      <div class="container box-1170">
        <div class="section-top-border">
          <h1 class="mt-10">Daftar</h1>
          <div class="row">
            <div class="col-lg-5 col-md-5 mr-10 mt-20 ">
                <form action="#">
                  <div class="mt-10">
                      <input type="text" name="first_name" placeholder="Nama Pengguna" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nama Pengguna'" required class="single-input">
                  </div>
                  <div class="mt-10">
                      <input type="email" name="EMAIL" placeholder="Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email'" required class="single-input">
                  </div>
                  <div class="mt-10">
                      <input type="email" name="EMAIL" placeholder="Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'" required class="single-input">
                  </div>
                  <div class="mt-10">
                      <input type="text" name="first_name" placeholder="Ketik Ulang Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Ketik Ulang Password'" required class="single-input">
                  </div>

                  <div class="mt-10">
                      <input type="email" name="EMAIL" placeholder="No Handphone" onfocus="this.placeholder = ''" onblur="this.placeholder = 'No Handphone'" required class="single-input">
                  </div>

                  <div class="mt-10">
                      <span class="mr-10">
                        <input  type="checkbox" name="Agree" id="Agree">
                      </span>
                      Saya setuju dengan <a href="#">Syarat dan Ketentuan </a> yang berlaku 
                  </div>
                  <div class="form-group mt-10">
                    <button type="submit" class="button button-contactForm">Daftar</button>
                  </div>
                </form>
            </div>
        </div>
        </div> 
      </div>
      
    </div>
   
    <!-- Form Element -->
    <!-- footer part start-->
    <?php $this->load->view("_Partials/Footer.php") ?>
    <!-- footer part end-->

    <?php $this->load->view("_Partials/Js.php") ?>
  </body>
</html>