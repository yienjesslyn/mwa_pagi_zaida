<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("_Partials/Head.php") ?>
	<!-- rating.js file -->
	<script src="<?php echo base_url('assets/MDB-Free_4.11.0/js/addons/rating.min.js')?>"></script>
</head>

<body>
	<!-- Preloader -->
	<?php $this->load->view("_Partials/Preloader.php") ?>

	<!-- Search Wrapper -->
	<?php $this->load->view("_Partials/SearchWrapper.php") ?>

	<!-- ##### Header Area Start ##### -->
	<?php $this->load->view("_Partials/HeaderArea.php") ?>
	<!-- ##### Header Area End ##### -->

	<!-- ##### Hero Area Start ##### -->
	<section class="hero-area top-catagory-area section-padding-80-20 mb-5">
		<div class="hero-slides owl-carousel ">
			<!-- Single Hero Slide -->
			<div class="single-hero-slide bg-img" style="background-image: url(<?= base_url() ?>assets/img/bg-img/otak.jpeg);">
				<div class="container h-100">
					<div class="row h-100 align-items-center">
						<div class="col-12 col-md-9 col-lg-7 col-xl-6">
							<div class="hero-slides-content" data-animation="fadeInUp" data-delay="100ms">
								<h2 data-animation="fadeInUp" data-delay="300ms">Fetival Kuliner Medan 2020 </h2>
								<p data-animation="fadeInUp" data-delay="700ms">Hadiri Pesta Kuliner Medan yang akan diadakan di Istana Maimun dan nikmati cashback hingga 30% dengan Gopay </p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Single Hero Slide -->
			<div class="single-hero-slide bg-img" style="background-image: url(<?= base_url() ?>assets/img/bg-img/bihun_bebek.jpg);">
				<div class="container h-100">
					<div class="row h-100 align-items-center">
						<div class="col-12 col-md-9 col-lg-7 col-xl-6">
							<div class="hero-slides-content" data-animation="fadeInUp" data-delay="100ms">
								<h2 data-animation="fadeInUp" data-delay="300ms">Kuliner Fest Dinas Pariwisata Kota Medan </h2>
								<p data-animation="fadeInUp" data-delay="700ms">Hadiri Pesta Kuliner Medan yang akan diadakan di Istana Maimun dan nikmati cashback hingga 30% dengan Ovo</p>
								<a href="#" class="btn delicious-btn" data-animation="fadeInUp" data-delay="1000ms">Lihat</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- ##### Hero Area End ##### -->

	<!-- ##### Blog Area Start ##### -->
	<div class="blog-area section-padding-80">
		<div class="container">
			<div class="row">
				<div class="col-8 col-md-8">
					<!-- Judul  -->
					<div class="receipe-headline">
						<h3>Daftar Promo Kuliner Medan dengan Gopay </h3>
						<!-- Single Preparation Step -->

						<style>
							a {
								font-size: 15pt;
							}

							a:hover {
								font-size: 15pt;
								color: green;
							}

							p {
								color: green;
							}
						</style>

						<p>
							<a href="<?php echo base_url('index.php/detailpenjual') ?>">Sate Padang Afrizal Amir - Cashback 10%</a>
							<br>
							<strong>Nikmati Ayam Krispi Favorit lebih hemat kalau bayar pakai Gopay</strong>
							<br>(1-31 Januari 2020)</p>

						<p>
							<a href="https://www.gojek.com/blog/promo-bengawan-solo/" target="_blank">Bengawan Solo - Voucher Cashback 20%</a>
							<br>
							<strong>Ngopi bareng teman atau sendiri lebih santai kalau bayar pakai GoPay</strong>
							<br>(1-31 Januari 2020)&nbsp;</p>

						<p>
							<a href="https://www.gojek.com/blog/promo-bengawan-solo/" target="_blank">Bengawan Solo - Voucher Cashback 20%</a>
							<br>
							<strong>Ngopi bareng teman atau sendiri lebih santai kalau bayar pakai GoPay</strong>
							<br>(1-31 Januari 2020)&nbsp;</p>

						<p>
							<a href="https://www.gojek.com/blog/promo-aw/" target="_blank">A&amp;W; - Cashback 10%</a>
							<br>
							<strong>Nikmati Ayam Krispi Favorit lebih hemat kalau bayar pakai GoPay</strong>
							<br>(1-31 Januari 2020)</p>

						<p>
							<a href="https://www.gojek.com/blog/promo-bengawan-solo/" target="_blank">Bengawan Solo - Voucher Cashback 20%</a>
							<br>
							<strong>Ngopi bareng teman atau sendiri lebih santai kalau bayar pakai GoPay</strong>
							<br>(1-31 Januari 2020)&nbsp;</p>


						<p>
							<a href="https://www.gojek.com/blog/promo-aw/" target="_blank">A&amp;W; - Cashback 10%</a>
							<br>
							<strong>Nikmati Ayam Krispi Favorit lebih hemat kalau bayar pakai GoPay</strong>
							<br>(1-31 Januari 2020)</p>


						<br>
						<hr>
						<hr>

						<h3>Daftar Promo Kuliner Medan dengan OVO </h3>
						<!-- Single Preparation Step -->

						<p>
							<a href="<?php echo base_url('index.php/detailpenjual') ?>" target="_blank">Sate Padang Afrizal Amir - Cashback 10%</a>
							<br>
							<strong>Nikmati Ayam Krispi Favorit lebih hemat kalau bayar pakai OVO</strong>
							<br>(1-31 Januari 2020)</p>

						<p>
							<a href="https://www.gojek.com/blog/promo-bengawan-solo/" target="_blank">Bengawan Solo - Voucher Cashback 20%</a>
							<br>
							<strong>Ngopi bareng teman atau sendiri lebih santai kalau bayar pakai OVO</strong>
							<br>(1-31 Januari 2020)&nbsp;</p>

						<p>
							<a href="https://www.gojek.com/blog/promo-bengawan-solo/" target="_blank">Bengawan Solo - Voucher Cashback 20%</a>
							<br>
							<strong>Ngopi bareng teman atau sendiri lebih santai kalau bayar pakai OVO</strong>
							<br>(1-31 Januari 2020)&nbsp;</p>

						<p>
							<a href="https://www.gojek.com/blog/promo-aw/" target="_blank">A&amp;W; - Cashback 10%</a>
							<br>
							<strong>Nikmati Ayam Krispi Favorit lebih hemat kalau bayar pakai OVO</strong>
							<br>(1-31 Januari 2020)</p>

						<p>
							<a href="https://www.gojek.com/blog/promo-bengawan-solo/" target="_blank">Bengawan Solo - Voucher Cashback 20%</a>
							<br>
							<strong>Ngopi bareng teman atau sendiri lebih santai kalau bayar pakai OVO</strong>
							<br>(1-31 Januari 2020)&nbsp;</p>


						<p>
							<a href="https://www.gojek.com/blog/promo-aw/" target="_blank">A&amp;W; - Cashback 10%</a>
							<br>
							<strong>Nikmati Ayam Krispi Favorit lebih hemat kalau bayar pakai OVO</strong>
							<br>(1-31 Januari 2020)</p>
					</div>
				</div>
			</div>
		</div>
		<!-- ##### Blog Area End ##### -->

		<section class="quote-subscribe-addsn mt-5">
			<div class="container">
				<div class="row align-items-end">
					<!-- ##### Follow Us Instagram Area Start ##### -->
					<?php $this->load->view("_Partials/GaleriEnd.php") ?>
					<!-- ##### Follow Us Instagram Area End ##### -->

					<!-- ##### Footer Area Start ##### -->
					<?php $this->load->view("_Partials/Footer.php") ?>
					<!-- ##### Footer Area Start ##### -->
				</div>
			</div>
		</section>

		<!-- ##### All Javascript Files ##### -->
		<?php $this->load->view("_Partials/Js.php") ?>
</body>

</html>