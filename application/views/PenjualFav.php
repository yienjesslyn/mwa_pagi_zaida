<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("_Partials/Head.php") ?>
</head>

<body>
  <!-- Preloader -->
  <?php $this->load->view("_Partials/Preloader.php") ?>

  <!-- Search Wrapper -->
  <?php $this->load->view("_Partials/SearchWrapper.php") ?>

  <!-- ##### Header Area Start ##### -->
  <?php $this->load->view("_Partials/HeaderArea.php") ?>
  <!-- ##### Header Area End ##### -->

  <!-- ##### Breadcumb Area Start ##### -->
  <div class="breadcumb-area bg-img bg-overlay" style="background-image: url(<?= base_url() ?>assets/img/bg-img/sate_padang.jpg);">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcumb-text text-center">
            <h2>Daftar Favorit</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ##### Breadcumb Area End ##### -->


  <!-- ##### Tabs ##### -->
  <section class="elements-area section-padding-80-0">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-12">
          <div class="delicious-tabs-content">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
              <!-- Tab 1  -->
              <li class="nav-item">
                <a class="nav-link active" id="tab--1" data-toggle="tab" href="#tab1" role="tab" aria-controls="tab1" aria-selected="false">Penjual Favorit</a>
              </li>
              <!-- Tab 2 -->
              <li class="nav-item">
                <a class="nav-link" id="tab--2" data-toggle="tab" href="#tab2" role="tab" aria-controls="tab2" aria-selected="false">Kuliner Favorit</a>
              </li>
              <!-- Tab 3 -->
              <li class="nav-item">
                <a class="nav-link" id="tab--3" data-toggle="tab" href="#tab3" role="tab" aria-controls="tab3" aria-selected="true">Artikel Favorit</a>
              </li>
            </ul>

            <div class="tab-content mb-80" id="myTabContent">

              <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab--1">
                <div class="delicious-tab-content">
                  <!-- Tab Text 1-->
                  <!-- Bagian Penjual -->
                  <div class="blog-area section-padding-20">
                    <div class="container">
                      <div class="row">

                        <!-- ##### Small Receipe Area Start ##### -->
                        <section class="small-receipe-area section-padding-20-0">
                          <div class="container">
                            <div class="row">

                              <!-- Small Receipe Area -->
                              <div class="col-12 col-sm-6 col-lg-6"  id="one">
                                <div class="single-small-receipe-area d-flex">
                                  <!-- Receipe Thumb -->
                                  <div class="receipe-thumb">
                                    <img src="<?php echo base_url('assets/img/bg-img/afrizal.jpg') ?>" alt="">
                                  </div>
                                  <!-- Receipe Content -->
                                  <div class="receipe-content">
                                    <span>January 04, 2018</span>
                                    <a href="<?php echo base_url('index.php/detailpenjual') ?>">
                                      <h5>Sate Padang Afrizal Amir</h5>
                                    </a>
                                    <div class="ratings">
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                    <p>2 Comments</p>
                                    <a id="hapusone" href="#">
                                      <p style="text-decoration: underline">Hapus dari Daftar Favorit</p>
                                    </a>
                                  </div>
                                  </div>
                              </div>

                              <!-- Small Receipe Area -->
                              <div class="col-12 col-sm-6 col-lg-6" id="two">
                                <div class="single-small-receipe-area d-flex">
                                  <!-- Receipe Thumb -->
                                  <div class="receipe-thumb">
                                    <img src="<?php echo base_url('assets/img/bg-img/ch.jpg') ?>" alt="">
                                  </div>
                                  <!-- Receipe Content -->
                                  <div class="receipe-content">
                                    <span>Maret 07, 2018</span>
                                    <a href="receipe-post.html">
                                      <h5>Mie Pangsit CH</h5>
                                    </a>
                                    <div class="ratings">
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                    <p>10 Comments</p>
                                    <a id="hapustwo" href="#">
                                      <p style="text-decoration: underline">Hapus dari Daftar Favorit</p>
                                  </div>
                                </div>
                              </div>

                              <!-- Small Receipe Area -->
                              <div class="col-12 col-sm-6 col-lg-6" id="one3">
                                <div class="single-small-receipe-area d-flex">
                                  <!-- Receipe Thumb -->
                                  <div class="receipe-thumb">
                                    <img src="<?php echo base_url('assets/img/bg-img/waspada.jpg') ?>" alt="">
                                  </div>
                                  <!-- Receipe Content -->
                                  <div class="receipe-content">
                                    <span>Desember 11, 2018</span>
                                    <a href="receipe-post.html">
                                      <h5>Sate Padang Simpang Waspada</h5>
                                    </a>
                                    <div class="ratings">
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                    <p>0 Comments</p>
                                    <a id="hapus3" href="#">
                                      <p style="text-decoration: underline">Hapus dari Daftar Favorit</p>
                                  </div>
                                </div>
                              </div>

                              <!-- Small Receipe Area -->
                              <div class="col-12 col-sm-6 col-lg-6" id="one4">
                                <div class="single-small-receipe-area d-flex">
                                  <!-- Receipe Thumb -->
                                  <div class="receipe-thumb">
                                    <img src="<?php echo base_url('assets/img/bg-img/joandah.jpg') ?>" alt="">
                                  </div>
                                  <!-- Receipe Content -->
                                  <div class="receipe-content">
                                    <span>September 09, 2018</span>
                                    <a href="receipe-post.html">
                                      <h5>Sate Padang Jo Andah</h5>
                                    </a>
                                    <div class="ratings">
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                    <p>2 Comments</p>
                                    <a id="hapus4" href="#">
                                      <p style="text-decoration: underline">Hapus dari Daftar Favorit</p>
                                  </div>
                                </div>
                              </div>

                              <!-- Small Receipe Area -->
                              <div class="col-12 col-sm-6 col-lg-6" id="one5">
                                <div class="single-small-receipe-area d-flex">
                                  <!-- Receipe Thumb -->
                                  <div class="receipe-thumb">
                                    <img src="<?php echo base_url('assets/img/bg-img/alfresco.jpg') ?>" alt="">
                                  </div>
                                  <!-- Receipe Content -->
                                  <div class="receipe-content">
                                    <span>Maret 14, 2018</span>
                                    <a href="receipe-post.html">
                                      <h5>Sate Padang Al'fresco</h5>
                                    </a>
                                    <div class="ratings">
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                    <p>2 Comments</p>
                                    <a id="hapus5" href="#">
                                      <p style="text-decoration: underline">Hapus dari Daftar Favorit</p>
                                  </div>
                                </div>
                              </div>

                              <!-- Small Receipe Area -->
                              <div class="col-12 col-sm-6 col-lg-6" id="one6">
                                <div class="single-small-receipe-area d-flex">
                                  <!-- Receipe Thumb -->
                                  <div class="receipe-thumb">
                                    <img src="<?php echo base_url('assets/img/bg-img/afrizal.jpg') ?>" alt="">
                                  </div>
                                  <!-- Receipe Content -->
                                  <div class="receipe-content">
                                    <span>Juni 22, 2018</span>
                                    <a href="receipe-post.html">
                                      <h5>Sate Padang H. Amiruddin (Bata)</h5>
                                    </a>
                                    <div class="ratings">
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                    <p>2 Comments</p>
                                    <a id="hapus6" href="#">
                                      <p style="text-decoration: underline">Hapus dari Daftar Favorit</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </section>
                        <!-- ##### Small Receipe Area End ##### -->
                      </div>
                    </div>
                  </div>
                  <!-- ##### Blog Area End ##### -->
                </div>
              </div>

              <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab--2">
                <div class="delicious-tab-content">
                  <!-- Tab Text 2 -->
                  <!-- Bagian Kuliner -->
                  <div class="blog-area section-padding-20">
                    <div class="container">
                      <div class="row">

                        <!-- ##### Small Receipe Area Start ##### -->
                        <section class="small-receipe-area section-padding-20-0">
                          <div class="container">
                            <div class="row">

                              <!-- Small Receipe Area -->
                              <div class="col-12 col-sm-6 col-lg-6">
                                <div class="single-small-receipe-area d-flex">
                                  <!-- Receipe Thumb -->
                                  <div class="receipe-thumb">
                                    <img src="<?php echo base_url('assets/img/bg-img/afrizal.jpg') ?>" alt="">
                                  </div>
                                  <!-- Receipe Content -->
                                  <div class="receipe-content">
                                    <span>January 04, 2018</span>
                                    <a href="<?php echo base_url('index.php/detailgizi') ?>">
                                      <h5>Sate Padang</h5>

                                    </a>
                                    <div class="ratings">
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                    <p>2 Comments</p>
                                    <a href="<?php echo base_url('index.php/detailgizi') ?>">
                                      <p style="text-decoration: underline">Hapus dari Daftar Favorit</p>
                                    </a>
                                  </div>
                                </div>
                              </div>

                              <!-- Small Receipe Area -->
                              <div class="col-12 col-sm-6 col-lg-6" id="pangsit">
                                <div class="single-small-receipe-area d-flex">
                                  <!-- Receipe Thumb -->
                                  <div class="receipe-thumb">
                                    <img src="<?php echo base_url('assets/img/bg-img/ch.jpg') ?>" alt="">
                                  </div>
                                  <!-- Receipe Content -->
                                  <div class="receipe-content">
                                    <span>January 04, 2018</span>
                                    <a href="receipe-post.html">
                                      <h5>Mie Pangsit</h5>
                                    </a>
                                    <div class="ratings">
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                    <p>10 Comments</p>
                                    <a id="pangsitdel" href="#">
                                      <p style="text-decoration: underline">Hapus dari Daftar Favorit</p>
                                  </div>
                                </div>
                              </div>

                              <!-- Small Receipe Area -->
                              <div class="col-12 col-sm-6 col-lg-6">
                                <div class="single-small-receipe-area d-flex">
                                  <!-- Receipe Thumb -->
                                  <div class="receipe-thumb">
                                    <img src="<?php echo base_url('assets/img/bg-img/sate_memeng.jpg') ?>" alt="">
                                  </div>
                                  <!-- Receipe Content -->
                                  <div class="receipe-content">
                                    <span>January 04, 2018</span>
                                    <a href="receipe-post.html">
                                      <h5>Sate Memeng</h5>
                                    </a>
                                    <div class="ratings">
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                    <p>2 Comments</p>
                                    <a href="<?php echo base_url('index.php/detailgizi') ?>">
                                      <p style="text-decoration: underline">Hapus dari Daftar Favorit</p>
                                  </div>
                                </div>
                              </div>

                              <!-- Small Receipe Area -->
                              <div class="col-12 col-sm-6 col-lg-6">
                                <div class="single-small-receipe-area d-flex">
                                  <!-- Receipe Thumb -->
                                  <div class="receipe-thumb">
                                    <img src="<?php echo base_url('assets/img/bg-img/buburay.jpeg') ?>" alt="">
                                  </div>
                                  <!-- Receipe Content -->
                                  <div class="receipe-content">
                                    <span>January 04, 2018</span>
                                    <a href="receipe-post.html">
                                      <h5>Bubur Ayam</h5>
                                    </a>
                                    <div class="ratings">
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                    <p>2 Comments</p>
                                    <a href="<?php echo base_url('index.php/detailgizi') ?>">
                                      <p style="text-decoration: underline">Hapus dari Daftar Favorit</p>
                                  </div>
                                </div>
                              </div>

                              <!-- Small Receipe Area -->
                              <div class="col-12 col-sm-6 col-lg-6">
                                <div class="single-small-receipe-area d-flex">
                                  <!-- Receipe Thumb -->
                                  <div class="receipe-thumb">
                                    <img src="<?php echo base_url('assets/img/bg-img/escampur.jpg') ?>" alt="">
                                  </div>
                                  <!-- Receipe Content -->
                                  <div class="receipe-content">
                                    <span>January 04, 2018</span>
                                    <a href="receipe-post.html">
                                      <h5>Es Teler</h5>
                                    </a>
                                    <div class="ratings">
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                    <p>2 Comments</p>
                                    <a href="<?php echo base_url('index.php/detailgizi') ?>">
                                      <p style="text-decoration: underline">Hapus dari Daftar Favorit</p>
                                  </div>
                                </div>
                              </div>

                              <!-- Small Receipe Area -->
                              <div class="col-12 col-sm-6 col-lg-6">
                                <div class="single-small-receipe-area d-flex">
                                  <!-- Receipe Thumb -->
                                  <div class="receipe-thumb">
                                    <img src="<?php echo base_url('assets/img/bg-img/otak.jpeg') ?>" alt="">
                                  </div>
                                  <!-- Receipe Content -->
                                  <div class="receipe-content">
                                    <span>January 04, 2018</span>
                                    <a href="receipe-post.html">
                                      <h5>Otak - Otak</h5>
                                    </a>
                                    <div class="ratings">
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                    <p>2 Comments</p>
                                    <a href="<?php echo base_url('index.php/detailgizi') ?>">
                                      <p style="text-decoration: underline">Hapus dari Daftar Favorit</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </section>
                        <!-- ##### Small Receipe Area End ##### -->
                      </div>
                    </div>
                  </div>
                  <!-- ##### Blog Area End ##### -->
                </div>
              </div>

              <div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab--3">
                <div class="delicious-tab-content">
                  <!-- Tab Text 3 -->
                  <!-- Bagian Artikel -->
                  <div class="blog-area section-padding-20">
                    <div class="container">
                      <div class="row">

                        <!-- ##### Small Receipe Area Start ##### -->
                        <section class="small-receipe-area section-padding-20-0">
                          <div class="container">
                            <div class="row">

                              <!-- Small Receipe Area -->
                              <div class="col-12 col-sm-6 col-lg-6">
                                <div class="single-small-receipe-area d-flex">
                                  <!-- Receipe Thumb -->
                                  <div class="receipe-thumb">
                                    <img src="<?php echo base_url('assets/img/bg-img/4sehat.jpg') ?>" alt="">
                                  </div>
                                  <!-- Receipe Content -->
                                  <div class="receipe-content">
                                    <span>January 04, 2018</span>
                                    <a href="<?php echo base_url('index.php/blog/artikel2') ?>">
                                      <h5>Yang Penting Gizi Seimbang, Bukan "4 Sehat 5 Sempurna"</h5>

                                    </a>
                                    <div class="ratings">
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                    <p>2 Comments</p>
                                    <a href="<?php echo base_url('index.php/detailgizi') ?>">
                                      <p style="text-decoration: underline">Hapus dari Daftar Favorit</p>
                                    </a>
                                  </div>
                                </div>
                              </div>

                              <!-- Small Receipe Area -->
                              <div class="col-12 col-sm-6 col-lg-6">
                                <div class="single-small-receipe-area d-flex">
                                  <!-- Receipe Thumb -->
                                  <div class="receipe-thumb">
                                    <img src="<?php echo base_url('assets/img/bg-img/jalankaki.jpg') ?>" alt="">
                                  </div>
                                  <!-- Receipe Content -->
                                  <div class="receipe-content">
                                    <span>January 04, 2018</span>
                                    <a href="<?php echo base_url('index.php/blog/artikel') ?>">
                                      <h5>Rutin Jalan Kaki Saat Hamil, Bunda Akan Merasakan 5 Manfaat ini</h5>
                                    </a>
                                    <div class="ratings">
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star" aria-hidden="true"></i>
                                      <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                    <p>10 Comments</p>
                                    <a href="<?php echo base_url('index.php/detailgizi') ?>">
                                      <p style="text-decoration: underline">Hapus dari Daftar Favorit</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </section>
                        <!-- ##### Small Receipe Area End ##### -->
                      </div>
                    </div>
                  </div>
                  <!-- ##### Blog Area End ##### -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    </div>
  </section>



  <section class="quote-subscribe-addsn mt-5">
    <div class="container">
      <div class="row align-items-end">
        <!-- ##### Follow Us Instagram Area Start ##### -->
        <?php $this->load->view("_Partials/GaleriEnd.php") ?>
        <!-- ##### Follow Us Instagram Area End ##### -->

        <!-- ##### Footer Area Start ##### -->
        <?php $this->load->view("_Partials/Footer.php") ?>
        <!-- ##### Footer Area Start ##### -->
      </div>
    </div>
  </section>

  <!-- ##### All Javascript Files ##### -->
  <?php $this->load->view("_Partials/Js.php") ?>

  <script>
    hapusone.addEventListener('click',function(){
      document.getElementById('one').remove()
    });
    pangsitdel.addEventListener('click',function(){
      document.getElementById('pangsit').remove()
    })
    
  </script>
</body>

</html>