<div class="search-wrapper">
  <!-- Close Btn -->
  <div class="close-btn"><i class="fa fa-times" aria-hidden="true"></i></div>

  <div class="container">
    <div class="row">
      <div class="col-12">
        <form method="post">
          <input id="keyword" action="<?php echo base_url('index.php/listkuliner') ?>" type="search" name="search" placeholder="Ketikkan kata kunci ...  ( contoh : nama kuliner, nama artikel, dll)">
          <button id="ic_search" type="submit" href="<?php echo base_url('index.php/listkuliner') ?>" ><i class="fa fa-search" aria-hidden="true"></i></button>
        </form>
      </div>
    </div>
  </div>
</div>


