<header class="header-area">

	<!-- Top Header Area -->
	<div class="top-header-area">
		<div class="container h-100">
			<div class="row h-100 align-items-center justify-content-between">
				<!-- Top Social Info -->
				<div class="col-12 col-sm-12">
					<div class="top-social-info text-right">
						<div class="login-btn">
						</div>
						<div class="search-btn">
							<i class="fa fa-search" aria-hidden="true"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Navbar Area -->
	<div class="delicious-main-menu">
		<div class="classy-nav-container breakpoint-off">
			<div class="container">
				<!-- Menu -->
				<nav class="classy-navbar justify-content-between" id="deliciousNav">

					<!-- Logo -->
					<a class="nav-brand" href="<?php echo base_url('/') ?>">
						<img src="<?php echo base_url('assets/img/core-img/nvidia.png') ?>" alt="">
					</a>
					<!-- Navbar Toggler -->
					<div class="classy-navbar-toggler">
						<span class="navbarToggler">
							<span></span>
							<span></span>
							<span></span>
						</span>
					</div>

					<!-- Menu -->
					<div class="classy-menu">

						<!-- close btn -->
						<div class="classycloseIcon">
							<div class="cross-wrap">
								<span class="top"></span>
								<span class="bottom"></span>
							</div>
						</div>

						<!-- Nav Start -->
						<div id="navigasi justify-content-center" class="classynav">
							<ul>
								<li class="nav" id="homeLink">
									<a href="<?php echo base_url('/') ?>">Beranda</a>
								</li>
								<li class="nav" id="blogLink">
									<a href="<?php echo base_url('post') ?>">Artikel</a>
								</li>
								<li id="categoryLink">
									<a href="#">Kategori</a>
									<ul class="dropdown">
										<li>
											<a onclick="semua()" href="<?php echo base_url('index.php/listkuliner') ?>">Semua</a>
										</li>
										<li>
											<a href="#">Jenis</a>
											<ul class="dropdown">
												<li>
													<a href="<?php echo base_url('index.php/makanan') ?>">Makanan</a>
												</li>
												<li>
													<a href="<?php echo base_url('index.php/minuman') ?>">Minuman</a>
												</li>
											</ul>
										</li>
										<li>
											<a href="#">Fitur</a>
											<ul class="dropdown">
												<li>
													<a href="<?php echo base_url('index.php/halal') ?>">Halal</a>
												</li>
												<li>
													<a href="<?php echo base_url('index.php/nonhalal') ?>">Non-Halal</a>
												</li>
												<li>
													<a href="<?php echo base_url('index.php/vege') ?>">Vegetarian</a>
												</li>
											</ul>
										</li>
									</ul>
								</li>
								<li class="nav" id="AskLink">
									<a href="<?php echo base_url('ask'); ?>">Tanya Ahli Gizi</a>
								</li>

								<li class="nav" id="FavLink">
									<a href="<?php echo base_url('jawab'); ?>">Buat Artikel</a>
								</li>

								<li class="nav" id="FavLink">
									<a href="<?php echo base_url('penjual'); ?>"> Favorit</a>
								</li>
								</li>
								<li class="nav" id="FavLink">
									<a href="<?php echo base_url('promo'); ?>">Promo</a>
								</li>
								</li>
								<li class="nav" id="contactLink">
									<a href="<?php echo base_url('contact'); ?>">Contact</a>
								</li>
								<li>
									<div class="search-btn">
									</div>
								</li>
							</ul>
						</div>
						<!-- Nav End -->
					</div>
				</nav>
			</div>
		</div>
	</div>
</header>

<script type="text/javascript">
	// Ganti Judul Jenis Kuliner berdasarkan kategori 
	function semua() {
		href = "<?php echo base_url('index.php/listkuliner') ?>"
		document.getElementById('JenisKuliner').innerText = "Semua Daftar Kuliner"
	}

	function makanan() {
		document.getElementById('JenisKuliner').innerText = "Daftar Kuliner Makanan"
	}

	function minuman() {
		href = "<?php echo base_url('index.php/blog/artikel1') ?>"
		document.getElementById('JenisKuliner').innerText = "Daftar Kuliner Minuman"
	}

	function halal() {
		href = "<?php echo base_url('index.php/listkuliner') ?>"
		document.getElementById('JenisKuliner').innerText = "Daftar Kuliner Halal"
	}

	function nonhalal() {
		href = "<?php echo base_url('index.php/listkuliner') ?>"
		document.getElementById('JenisKuliner').innerText = "Daftar Kuliner Non-Halal"
	}

	function vege() {
		href = "<?php echo base_url('index.php/listkuliner') ?>"
		document.getElementById('JenisKuliner').innerText = "Daftar Kuliner Vegetarian"
	}


	// Aktifkan Link 
	var pageTitle = "<?php echo $_pageTitle ?>"
	if (pageTitle == "Home") {
		document.getElementById('homeLink').classList.add('active')
	} else if (pageTitle == "About") {
		document.getElementById('aboutLink').classList.add('active')
	} else if (pageTitle == "Blog") {
		document.getElementById('blogLink').classList.add('active')
	} else if (pageTitle == "Contact") {
		document.getElementById('contactLink').classList.add('active')
	} else if (pageTitle == "DaftarKuliner") {
		document.getElementById('categoryLink').classList.add('active')
	} else if (pageTitle == "DetailGizi") {
		document.getElementById('categoryLink').classList.add('active')
	} else if (pageTitle == "DetailPenjual") {
		document.getElementById('categoryLink').classList.add('active')
	} else if (pageTitle == "Artikel") {
		document.getElementById('blogLink').classList.add('active')
	} else if (pageTitle == "Artikel2") {
		document.getElementById('blogLink').classList.add('active')
	} else if (pageTitle == "PenjualFav") {
		document.getElementById('FavLink').classList.add('active')
	} else if (pageTitle == "TanyaJawab") {
		document.getElementById('AskLink').classList.add('active')
	} else if (pageTitle == "Promo") {
		document.getElementById('Promolink').classList.add('active')
	}
</script>